*.txt
.git
*.*~
- var/cache/*
- var/logs/*
- var/sessions/*
- web/.htaccess
- web/uploads/*
- app/config/parameters.yml

