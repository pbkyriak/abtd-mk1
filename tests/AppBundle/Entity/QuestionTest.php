<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\ArticleReview;
use AppBundle\Entity\QuestionAnswer;
use AppBundle\Entity\QuestionMultiple;
use AppBundle\Entity\QuestionNumber;
use AppBundle\Entity\QuestionSingle;
use AppBundle\Entity\QuestionText;
use AppBundle\Transformer\BibtexToArticle;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use AppBundle\Entity\Article;
use AppBundle\Entity\Question;
use AppBundle\Entity\ReviewQuestion;
use AppBundle\Parser\BibTexParser;



class QuestionTest extends KernelTestCase {
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    private $validator;

    /** @var  \AppBundle\Transformer\BibtexToArticle */
    private $trans;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
    }
    /**
     * {@inheritDoc}
     */
    protected function setUp() {

        $this->em = self::$kernel->getContainer()
                                 ->get('doctrine')
                                 ->getManager();
        $this->trans = self::$kernel->getContainer()->get('transformer.bibtex_to_article');
        $this->validator = self::$kernel->getContainer()->get('validator');
    }

    public function testQuestionSingle() {
        $q = new QuestionSingle();
        $q->setQuestion("test 1");
        $q->setActive(true);

        $a1 = new QuestionAnswer();
        $a1->setTitle('ans t 1');
        $a1->setQuestion($q);
        $a2 = new QuestionAnswer();
        $a2->setTitle('ans t 2');
        $a2->setQuestion($q);

        $this->em->persist($q);
        $this->em->flush();
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown() {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}