<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\ArticleReview;
use AppBundle\Entity\QuestionMultiple;
use AppBundle\Entity\QuestionNumber;
use AppBundle\Entity\QuestionSingle;
use AppBundle\Entity\QuestionText;
use AppBundle\Transformer\BibtexToArticle;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use AppBundle\Entity\Article;
use AppBundle\Entity\Question;
use AppBundle\Entity\ReviewQuestion;
use AppBundle\Parser\BibTexParser;

class ArticleTest extends KernelTestCase {
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    private $validator;

    /** @var  \AppBundle\Transformer\BibtexToArticle */
    private $trans;

    public static function setUpBeforeClass()
    {
        self::bootKernel();
    }
    /**
     * {@inheritDoc}
     */
    protected function setUp() {

        $this->em = self::$kernel->getContainer()
                           ->get('doctrine')
                           ->getManager();
        $this->trans = self::$kernel->getContainer()->get('transformer.bibtex_to_article');
        $this->validator = self::$kernel->getContainer()->get('validator');
    }

    private function createNewArticle($bibtexFile) {

        try {
            $bibtex = file_get_contents(__DIR__.'/../Bibtex/UseCases/'.$bibtexFile);
            $article = $this->trans->toArticle($bibtex);
            $article->setStatus('D');
        }
        catch(\Exception $ex) {
            printf("ex:%s\n", $ex->getMessage());
            return false;
        }
        $errors = $this->validator->validate($article);
        if(count($errors)) {
            echo (string)$errors;
            return false;
        }
        else {
            printf("\n VALID!");
            $this->em->persist($article);
            $this->em->flush();
            $this->assertGreaterThan(0, $article->getId());
            return $article->getId();
        }
    }

    private function createNewReview(Article $article) {
        $questions = $this->em->getRepository('AppBundle:Question')->findAll();
        $this->assertCount(4, $questions);

        $review = new ArticleReview();
        $review->setArticle($article);
        $article->setReview($review);
        $review->setReviewText('testing');

        foreach($questions as $question) {
            $aq = new ReviewQuestion();
            $aq->setReview($review);
            $aq->setQuestion($question);
            $review->addQuestion($aq);
        }

        $this->assertCount(4, $review->getQuestions());

        foreach($review->getQuestions() as $aq) {
            if( $aq->getQuestion() instanceof  QuestionNumber) {
                $aq->setAnswerN(12);
            }
            elseif( $aq->getQuestion() instanceof  QuestionText) {
                $aq->setAnswerT('text');
            }
            elseif( $aq->getQuestion() instanceof  QuestionSingle) {
                $answers = $aq->getQuestion()->getAnswers();
                //$this->assertCount(3, $answers);
                //$aq->addAnswer($answers[1]);
            }
            elseif( $aq->getQuestion() instanceof  QuestionMultiple) {
                $answers = $aq->getQuestion()->getAnswers();
                //$this->assertCount(3, $answers);
                //$aq->addAnswer($answers[1]);
                //$aq->addAnswer($answers[3]);
            }
        }


        $this->em->persist($article);
        $this->em->flush();
    }

    public function testCreate() {
        $newId = $this->createNewArticle('bibtex-1.bib');
        //$newId = $this->createNewArticle('bibtex-2.bib');
        //$newId = $this->createNewArticle('bibtex-3.bib');
        //$newId = $this->createNewArticle('bibtex-3.bib');
        if($newId) {
            $article2 = $this->em->getRepository("AppBundle:Article")->find($newId);

            $this->createNewReview($article2);
            //$this->assertCount(4, $article2->getReview()->getQuestions());
        }
    }

    public function NOtestDeleteQ() {
        $question = $this->em->getRepository('AppBundle:Question')->find(47);
        $this->em->remove($question);
        $this->em->flush();

        $questions = $this->em->getRepository('AppBundle:Question')->findAll();
        $this->assertCount(4, $questions);

        $article2 = $this->em->getRepository('AppBundle:Article')->find(15);
        $this->assertCount(4, $article2->getQuestions());
    }

    public function NOtestUpdateQA() {
        $newId = $this->createNewArticle();
        $article = $this->em->getRepository('AppBundle:Article')->find($newId);
        $this->assertCount(5, $article->getQuestions());
        foreach($article->getQuestions() as $aq) {
            if( $aq->getQuestion() instanceof  QuestionNumber) {
                $aq->setAnswerN(222);
            }
            elseif( $aq->getQuestion() instanceof  QuestionText) {
                $aq->setAnswerT('text222');
            }
            elseif( $aq->getQuestion() instanceof  QuestionSingle) {
                $answers = $aq->getQuestion()->getAnswers();
                foreach($aq->getAnswer() as $aqa) {
                    $aq->removeAnswer($aqa);
                }
                $aq->addAnswer($answers[0]);
            }
            elseif( $aq->getQuestion() instanceof  QuestionMultiple) {
                $answers = $aq->getQuestion()->getAnswers();
                foreach($aq->getAnswer() as $aqa) {
                    $aq->removeAnswer($aqa);
                }
                $aq->addAnswer($answers[0]);
                $aq->addAnswer($answers[4]);
            }

        }

        $this->em->persist($article);
        $this->em->flush();

    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown() {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}