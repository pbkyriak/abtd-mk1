<?php

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use RenanBr\BibTexParser\Parser;
use RenanBr\BibTexParser\Listener;
use RenanBr\BibTexParser\Processor\TagNameCaseProcessor;
use RenanBr\BibTexParser\Processor\NamesProcessor;
use RenanBr\BibTexParser\Processor\KeywordsProcessor;

class BidtetParseTest extends \PHPUnit_Framework_TestCase  {

    public function NOtestCrossref() {

        $client = new \AppBundle\Client\CrossRefOrgClient();
        $data  = $client->get('10.1002/smr.402');
        print_r($data);
        $tr = new \AppBundle\Transformer\CrossRefOrgReferencesToEntities();
        $tr->trans($data["reference"]);
    }

    public function NOtestParseOne() {

        //
        $bibtex = file_get_contents(__DIR__.'/UseCases/bibtex-4.bib');
        $entries = $this->parse($bibtex);

        $this->dump($entries[0]);

        $bibtex = file_get_contents(__DIR__.'/UseCases/bibtex-1.bib');
        $entries = $this->parse($bibtex);
        //print_r($entries);
        $this->dump($entries[0]);

        $bibtex = file_get_contents(__DIR__.'/UseCases/bibtex-2.bib');
        $entries = $this->parse($bibtex);
        //print_r($entries);
        $this->dump($entries[0]);

        $bibtex = file_get_contents(__DIR__.'/UseCases/bibtex-3.bib');
        $entries = $this->parse($bibtex);
        $this->dump($entries[0]);

        $this->assertEquals(1,1);
    }
    public function testParseTwo() {

        //
        $bibtex = file_get_contents(__DIR__ . '/UseCases/bibtex-6.bib');
        $entries = $this->parse($bibtex);
var_dump($entries[0]['AUTHOR']);
//        $this->dump($entries[0]);
    }
    private function parse($bibtex) {
        $parser = new Parser();          // Create a Parser
        $listener = new Listener();      // Create and configure a Listener
        $listener->addProcessor(new \AppBundle\Parser\ParserProcessor\LatexResolverProcessor());
        $listener->addProcessor(new TagNameCaseProcessor(CASE_UPPER)); // or CASE_LOWER
        $listener->addProcessor(new NamesProcessor());
        $listener->addProcessor(new KeywordsProcessor());
        $parser->addListener($listener); // Attach the Listener to the Parser
        //
        $parser->parseString($bibtex);   // or parseFile('/path/to/file.bib')
        $entries = $listener->export();  // Get processed data from the Listener
        return $entries;
    }

    private function dump($entries) {
        printf("\n=============================\n");
        printf("type: %s\n", $entries['TYPE']);
        printf("title: %s\n", $entries['TITLE']);
        if(isset($entries['DATE'])) {
            printf("Date: %s\n", $entries['DATE']);
        }
        else {
            printf("Date: %s\n", $entries['YEAR']);
        }
        printf("Author(s):\n");
        print_r($entries['AUTHOR']);
        foreach($entries['AUTHOR'] as $a) {
            printf("\t%s\n", implode(',', $a));
        }
        if($entries['TYPE']=='inproceedings') {
            printf("PUBLISHER: %s\n", !empty($entries['PUBLISHER']) ? $entries['PUBLISHER'] : '');
            printf("BOOKTITLE: %s\n", !empty($entries['BOOKTITLE']) ? $entries['BOOKTITLE'] : '');
            if (!empty($entries['KEYWORDS'])) {
                printf("Tag(s): %s\n", implode(',', $entries['KEYWORDS']));
            }
        }
        elseif($entries['TYPE']=='article') {
            printf("DOI: %s\n", !empty($entries['DOI']) ? $entries['DOI'] : '');
            printf("URL: %s\n", !empty($entries['URL']) ? $entries['URL'] : '');
            printf("JOURNALTITLE: %s\n", !empty($entries['JOURNALTITLE']) ? $entries['JOURNALTITLE'] : '');
            if(!empty($entries['KEYWORDS'])) {
                printf("Tag(s): %s\n", implode(',', $entries['KEYWORDS']));
            }
        }
        elseif($entries['TYPE']=='book') {
            printf("PUBLISHER: %s\n", !empty($entries['PUBLISHER']) ? $entries['PUBLISHER'] : '');
        }
    }
}