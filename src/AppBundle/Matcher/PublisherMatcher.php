<?php

namespace AppBundle\Matcher;


use AppBundle\Entity\Publisher;

class PublisherMatcher implements MatcherInterface  {
    private $repo;

    public function __construct($repo) {
        $this->repo = $repo;
    }

    public function matchOrCreate(array $params) {
        $title = $params['title'];

        $k = $this->repo->getByTitle($title);
        if(!$k) {
            $k = new Publisher();
            $k->setTitle($title);
        }
        return $k;
    }
}