<?php

namespace AppBundle\Matcher;

use AppBundle\Entity\Keyword;

class KeywordMatcher implements MatcherInterface {

    private $keywordsRepo;

    public function __construct($keywordsRepo) {
        $this->keywordsRepo = $keywordsRepo;
    }

    public function matchOrCreate(array $params) {
        $title = $params['title'];
        $k = $this->keywordsRepo->getKeywordByTitle($title);
        if(!$k) {
            $k = new Keyword();
            $k->setTitle($title);
        }
        return $k;
    }
}