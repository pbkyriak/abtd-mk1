<?php

namespace AppBundle\Matcher;

interface MatcherInterface {
    public function matchOrCreate(array $params);
}