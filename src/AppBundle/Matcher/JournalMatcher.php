<?php

namespace AppBundle\Matcher;

use AppBundle\Entity\Journal;

class JournalMatcher implements MatcherInterface {
    private $repo;

    public function __construct($repo) {
        $this->repo = $repo;
    }

    public function matchOrCreate(array $params) {
        $title = $params['title'];

        $k = $this->repo->getByTitle($title);
        if(!$k) {
            $k = new Journal();
            $k->setTitle($title);
        }
        return $k;
    }
}