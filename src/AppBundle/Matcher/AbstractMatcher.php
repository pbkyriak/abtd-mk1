<?php

namespace AppBundle\Matcher;

use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractMatcher implements MatcherInterface {

    protected $resolver;

    final public function setupResolver() {
        $this->resolver = new OptionsResolver();
        $this->configureOptions();
    }

    abstract protected function configureOptions();
    abstract public function matchOrCreate(array $params);


}