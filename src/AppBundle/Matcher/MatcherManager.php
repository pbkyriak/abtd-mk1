<?php

namespace AppBundle\Matcher;

class MatcherManager {

    private $matchers;

    public function __construct() {
        $this->matchers = [];
    }

    public function setMatcher($key, MatcherInterface $matcher) {
        if( method_exists($matcher, 'setupResolver') ) {
            $matcher->setupResolver();
        }
        $this->matchers[$key] = $matcher;
    }

    public function getMatchers() {
        return $this->matchers;
    }

    /**
     * @param $key
     * @return MatcherInterface|null
     */
    public function get($key) {
        if(isset($this->matchers[$key])) {
            return $this->matchers[$key];
        }
        return null;
    }
}