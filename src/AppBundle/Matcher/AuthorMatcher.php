<?php

namespace AppBundle\Matcher;


use AppBundle\Entity\Author;
use AppBundle\Entity\Repository\AuthorRepository;

class AuthorMatcher extends AbstractMatcher {
    private $repo;

    public function __construct(AuthorRepository  $repo) {
        $this->repo = $repo;
    }

    protected function configureOptions() {
        $this->resolver->setDefaults(
            [
                'family' => '',
                'given' => '',
                'affiliation' => '',
            ]
        );
    }

    public function matchOrCreate(array $params) {
        $values = $this->resolver->resolve($params);
        $family = $values['family'];
        $given = $values['given'];
        $affiliation = $values['affiliation'];

        $k = $this->repo->getByFullName($family, $given, $affiliation);
        if(!$k) {
            $k = new Author();
            $k->setFamily($family)
                ->setGiven($given)
                ->setAffiliation($affiliation);
        }
        return $k;
    }
}