<?php

namespace AppBundle;

use AppBundle\DependencyInjection\Compiler\ArticleFilterFormPass;
use AppBundle\DependencyInjection\Compiler\EntityMatcherPass;
use AppBundle\DependencyInjection\Compiler\QuestionAnswerFieldsPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use AppBundle\DependencyInjection\Compiler\BibtexTransformerPass;
use AppBundle\DependencyInjection\Compiler\ArticleFiltersPass;
use AppBundle\DependencyInjection\Compiler\RptRegistersPass;

class AppBundle extends Bundle {

    public function build(ContainerBuilder $container) {
        $container->addCompilerPass(new BibtexTransformerPass());
        $container->addCompilerPass(new EntityMatcherPass());
        $container->addCompilerPass(new QuestionAnswerFieldsPass());
        $container->addCompilerPass(new ArticleFiltersPass());
        $container->addCompilerPass(new RptRegistersPass());
    }
}
