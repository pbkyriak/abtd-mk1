<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseProfileFormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ProfileFormType  extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('firstName', null, ['required'=>true])
            ->add('lastName', null, ['required'=>true])
            ->add('phone', null, ['required'=>true])
            ->add('save', SubmitType::class, array('label' => 'users.general.apply', 'attr' => array('class' => 'btn blue')))
        ;
        $builder->remove('current_password');
    }

    public function getParent() {
        return BaseProfileFormType::class;
    }

}