<?php

namespace AppBundle\Form\Listener;

use AppBundle\Entity\QuestionAnswer;
use AppBundle\Entity\QuestionSingleAnswer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class AddReviewQuestionSingleFieldListener implements EventSubscriberInterface {
    public static function getSubscribedEvents() {
        return array(
            FormEvents::PRE_SET_DATA => 'onPreSetData',
        );
    }

    public function onPreSetData(FormEvent $event) {
        $reviewQuestion = $event->getData();
        $form = $event->getForm();
        if ($reviewQuestion->getQuestion()) {
            if ($reviewQuestion->getQuestion() instanceof \AppBundle\Entity\QuestionSingle) {
                $form->add(
                    'answer_s',
                    EntityType::class,
                    [
                        'class' => QuestionSingleAnswer::class,
                        'query_builder' => function (EntityRepository $er) use ($reviewQuestion) {
                            return $er->createQueryBuilder('a')
                                      ->where('a.question=:q')
                                      ->setParameter('q', $reviewQuestion->getQuestion());
                        },
                        'label' => 'number',
                        'expanded' => false,
                        'attr' => ['class' => 'answer_input'],
                    ]
                );
            }
        }
    }
}