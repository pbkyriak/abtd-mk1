<?php

namespace AppBundle\Form\Listener;

use AppBundle\Entity\QuestionAnswer;
use AppBundle\Entity\QuestionMultiple;
use AppBundle\Entity\ReviewQuestion;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class AddReviewQuestionMultipleFieldListener implements EventSubscriberInterface {

    private $em;
    private $questionIDs = [];
    private $curQuestionIdx = -1;

    public static function getSubscribedEvents() {
        return array(
            FormEvents::PRE_SET_DATA => 'onPreSetData',
            FormEvents::PRE_SUBMIT => 'onPreSubmit',
//            FormEvents::SUBMIT => 'onSubmit',
        );
    }

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function onPreSetData(FormEvent $event) {
        $reviewQuestion = $event->getData();
        $form = $event->getForm();
        if ($reviewQuestion->getQuestion()) {
            if ($reviewQuestion->getQuestion() instanceof \AppBundle\Entity\QuestionMultiple) {
                $this->questionIDs[] = $reviewQuestion->getQuestion()->getId();
                $form->add(
                    'answer',
                    EntityType::class,
                    [
                        'class' => QuestionAnswer::class,
                        'query_builder' => function (EntityRepository $er) use ($reviewQuestion) {
                            return $er->createQueryBuilder('a')
                                      ->where('a.question=:q')
                                      ->setParameter('q', $reviewQuestion->getQuestion());
                        },
                        'label' => 'number',
                        'multiple' => true,
                        'attr' => [
                            'class' => 'js-select-multi-tags answer_input',
                            'style' => 'width:100%',
                            'data-question' => $reviewQuestion->getQuestion()->getId(),
                        ]
                    ]
                );
            }
        }
    }

    public function onPreSubmit(FormEvent $event) {
        $data = $event->getData();
        if(isset($data['answer'])) {
            $this->curQuestionIdx++;
//            printf("Question id =%s <Br />", $this->questionIDs[$this->curQuestionIdx]);
            foreach($data['answer'] as $k => $v) {
                if(substr($v,0,8)==='#CREATE#') {
//                    printf("create new keyword %s <Br />", $v);
                    $word = substr($v, 8);
                    $question = $this->em->getRepository("AppBundle:QuestionMultiple")->find($this->questionIDs[$this->curQuestionIdx]);
                    if( $question ) {
                        $ans = new QuestionAnswer();
                        $ans->setTitle($word);
                        $ans->setQuestion($question);
                        $this->em->persist($ans);
                        $this->em->flush($ans);
                        unset($data['answer'][$k]);
                        $data['answer'][]= $ans->getId();
                    }
                }
            }
            $event->setData($data);
        }
    }
/*
    public function onSubmit(FormEvent $event) {
        printf("<br />EVENT: SUBMIT<br />");
        $data = $event->getData();
        if(is_object($data)) {
            printf("%s<br />", get_class($data));
        }
        if($data instanceof  ReviewQuestion) {
        if($data->getQuestion() instanceof  QuestionMultiple) {
            $as = $data->getAnswer();
            printf("count = %s<br />", count($as));
            foreach ($as as $a) {
                printf("== a = %s %s<br />", $a->getTitle(), $a->getId());
            }
        }
        }
        printf("<br />============================<br />");

    }
*/
}