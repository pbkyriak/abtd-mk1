<?php

namespace AppBundle\Form\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AddReviewQuestionTextFieldListener implements EventSubscriberInterface {

    public static function getSubscribedEvents() {
        return array(
            FormEvents::PRE_SET_DATA => 'onPreSetData',
        );
    }

    public function onPreSetData(FormEvent $event) {
        $reviewQuestion = $event->getData();
        $form = $event->getForm();
        if($reviewQuestion->getQuestion()) {
            if ($reviewQuestion->getQuestion() instanceof \AppBundle\Entity\QuestionText) {
                $form->add(
                    'answer_t',
                    TextType::class,
                    [
                        'label' => 'text',
                        'attr' => ['class' => 'answer_input'],
                    ]
                );
            }
        }
    }
}