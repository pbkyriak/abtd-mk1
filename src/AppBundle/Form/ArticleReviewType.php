<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Slx\MetronicBundle\Form\Type\TabType;

class ArticleReviewType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        /*
        $generalTab = $builder->create('general',TabType::class,[
            'label' => 'General',
            'icon' => 'pencil',
            'inherit_data' => true,
        ]);
        $rulesTab = $builder->create('rules',TabType::class,[
            'label' => 'Questions',
            'icon' => 'pencil',
            'inherit_data' => true,
        ]);

        $generalTab
            ->add('review_text', TextareaType::class, ['required' => true, 'label' => 'review_text', 'help'=>'abtd.review.review_text_help'])
        ;
        $rulesTab
            ->add(
                'questions',
                CollectionType::class,
                [
                    'entry_type'=>ReviewQuestionType::class,
                    'label'=>'Questions',
//                    'allow_add' => false,
//                    'allow_delete' => false,
                ]
            )
        ;
        $builder
            ->add($generalTab)
            ->add($rulesTab)
            ->add('save', SubmitType::class, array('label' => 'slx_metronic.general.save', 'attr' => array('class' => 'btn blue')))
            ->add('saveAndAdd', SubmitType::class, array('label' => 'slx_metronic.general.save_and_add', 'attr' => array('class' => 'btn blue')))

        ;
        */
        $builder
            ->add('review_text', TextareaType::class, ['required' => true, 'label' => 'review_text', 'help'=>'abtd.review.review_text_help'])
            ->add(
                'questions',
                CollectionType::class,
                [
                    'entry_type'=>ReviewQuestionType::class,
                    'label'=>'Questions',
//                    'allow_add' => false,
//                    'allow_delete' => false,
                ]
            )
            ->add('save', SubmitType::class, array('label' => 'slx_metronic.general.save', 'attr' => array('class' => 'btn blue')))
//            ->add('saveAndAdd', SubmitType::class, array('label' => 'slx_metronic.general.save_and_add', 'attr' => array('class' => 'btn blue')))
            ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
                                   'data_class' => 'AppBundle\Entity\ArticleReview'
                               ));
    }

    public function getName() {
        return 'review';
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'review';
    }


}