<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseProfileFormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserFormType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $choices = [
            'ROLE_SUPER_ADMIN' => 'ROLE_SUPER_ADMIN',
            'ROLE_ADMIN'=>'ROLE_ADMIN',
            'ROLE_EDITOR'=>'ROLE_EDITOR',
            'ROLE_USER'=>'ROLE_USER',

        ];
        $builder
            ->add('firstName', null, ['required'=>true])
            ->add('lastName', null, ['required'=>true])
            ->add('phone', null, ['required'=>true])
            ->add('enabled', null)
            ->add('plainPassword', null, ['required'=>false])
            ->add('roles', ChoiceType::class, array('choices'=>$choices,'label'=>'users.user.roles', 'multiple'=>true))
            ->add('save', SubmitType::class, array('label' => 'users.general.apply', 'attr' => array('class' => 'btn blue')))
        ;
        $builder->remove('current_password');
    }

    public function getParent() {
        return BaseProfileFormType::class;
    }

}