<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleCommentFormType  extends AbstractType  {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add(
                'comment_text',
                TextareaType::class,
                ['label'=>'app.article.comment', 'required'=>true]
            )
            ->add(
                'save',
                SubmitType::class,
                array(
                    'label' => 'app.article.submit',
                    'attr' => array('class' => 'btn blue uppercase')
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(['data_class' => 'AppBundle\Entity\ArticleComment']);
    }

    public function getName() {
        return 'comment';
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'comment';
    }

}