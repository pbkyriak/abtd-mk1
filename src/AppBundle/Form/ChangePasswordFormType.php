<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use FOS\UserBundle\Form\Type\ChangePasswordFormType as BaseChangePasswordFormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ChangePasswordFormType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('save', SubmitType::class, array('label' => 'users.general.apply', 'attr' => array('class' => 'btn blue')))
            ;
    }

    public function getParent() {
        return BaseChangePasswordFormType::class;
    }

}