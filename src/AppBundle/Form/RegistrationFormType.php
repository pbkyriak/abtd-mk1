<?php

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegistrationFormType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('firstName', null, ['required'=>true])
            ->add('lastName', null, ['required'=>true])
            ->add('phone', null, ['required'=>true])
            ->add('save', SubmitType::class, array('label' => 'users.general.register_submit', 'attr' => array('class' => 'btn blue')))
        ;

    }

    public function getParent() {
        return BaseRegistrationFormType::class;
    }
}