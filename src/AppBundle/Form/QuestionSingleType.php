<?php

namespace AppBundle\Form;

use AppBundle\Entity\QuestionAnswer;
use AppBundle\Entity\QuestionSingleAnswer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\Common\Collections\Collection;

class QuestionSingleType  extends AbstractType  {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('question', TextType::class, ['label'=>'app.questions.question'])
            ->add('active', null,['label'=>'slx.general.active'])
            ->add('show_filter', null,['label'=>'app.questions.show_filter'])
            ->add('answers', CollectionType::class, ['entry_type'=> QuestionSingleAnswerType::class, 'allow_add'=>true, 'allow_delete'=>true, 'by_reference'=>false])
            ->add('save', SubmitType::class, array('label' => 'slx_metronic.general.save', 'attr' => array('class' => 'btn blue')))
            ->add('saveAndAdd', SubmitType::class, array('label' => 'slx_metronic.general.save_and_add', 'attr' => array('class' => 'btn blue')))

        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\QuestionSingle'
            )
        );
    }

    public function getName() {
        return 'question';
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'question';
    }

}