<?php

namespace AppBundle\Form;

use AppBundle\Form\Listener\FltArticleReviewDocTypesListener;
use AppBundle\Form\Listener\FltArticleReviewQuestionsListener;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FltArticleReviewType extends AbstractType {
    private $subscribers=[];

    public function buildForm(FormBuilderInterface $builder, array $options) {
        foreach($this->subscribers as $subscriber) {
            if(is_object($subscriber)) {
                $builder->addEventSubscriber($subscriber);
            }
            else {
                $builder->addEventSubscriber(new $subscriber());
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(
            array(
                'csrf_protection' => false,
            )
        );

    }

    public function getName() {
        return 'fltreview';
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'fltreview';
    }

    public function addFormSubscriber($type,$listener) {
        $this->subscribers[$type] = $listener;
    }

}