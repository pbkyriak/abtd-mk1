<?php

namespace AppBundle\Form\ChoiceLoader;

use Symfony\Component\Form\ChoiceList\ChoiceListInterface;
use Symfony\Component\Form\ChoiceList\Loader\ChoiceLoaderInterface;
use Symfony\Component\Form\FormEvent;

/**
 * Class AnswerChoiceLoader
 *
 * maybe useless
 * idea from https://www.strehle.de/tim/weblog/archives/2016/02/24/1588
 *
 * @package AppBundle\Form\ChoiceLoader
 */
class AnswerChoiceLoader implements ChoiceLoaderInterface {

    protected $selected;

    /**
     * Constructor
     */
    public function __construct($builder) {
        if (is_object($builder) && ($builder instanceof FormBuilderInterface)) {
            // Let the form builder notify us about initial/submitted choices

            $builder->addEventListener
            (
                FormEvents::POST_SET_DATA,
                [$this, 'onFormPostSetData']
            );

            $builder->addEventListener
            (
                FormEvents::POST_SUBMIT,
                [$this, 'onFormPostSetData']
            );
        }
    }

    /**
     * Form submit event callback
     * Here we get notified about the submitted choices.
     * Remember them so we can add them in loadChoiceList().
     */
    public function onFormPostSetData(FormEvent $event) {
        $this->selected = [];

        $formdata = $event->getData();

        if (!is_object($formdata)) {
            return;
        }

        $this->selected = $formdata->tags;
    }

    /**
     * Choices to be displayed in the SELECT element.
     * It's okay to not return all available choices, but the
     * selected/submitted choices (model values) must be
     * included.
     * Required by ChoiceLoaderInterface.
     */
    public function loadChoiceList($value = null) {
        // Get first n choices

        $choices = $this->getChoicesList(false);

        // Check which choices are missing

        $missing_choices = array_flip($this->selected);

        foreach ($choices as $label => $id) {
            if (isset($missing_choices[$id])) {
                unset($missing_choices[$id]);
            }
        }

        // Now add selected choices if they're missing

        foreach (array_keys($missing_choices) as $id) {
            $label = $this->getChoiceLabel($id);

            if (strlen($label) === 0) {
                continue;
            }

            $choices[$label] = $id;
        }

        return new ArrayChoiceList($choices);
    }

    /**
     * Validate submitted choices, and turn them from strings
     * (HTML option values) into other datatypes if needed
     * (not needed here since our choices are strings).
     * We're also using this place for creating new choices
     * from new values typed into the autocomplete field.
     * Required by ChoiceLoaderInterface.
     */
    public function loadChoicesForValues(array $values, $value = null) {
        $result = [];

        foreach ($values as $id) {
            if (substr($id, 0, 7) === 'create:') {
                $label = substr($id, 7);
                $result[] = $this->createChoice($label);
            }
            elseif ($this->choiceExists($id)) {
                $result[] = $id;
            }
        }

        return $result;
    }

    /**
     * Turn choices from other datatypes into strings (HTML option
     * values) if needed - we can simply return the choices as
     * they're strings already.
     * Required by ChoiceLoaderInterface.
     */
    public function loadValuesForChoices(array $choices, $value = null) {
        $result = [];

        foreach ($choices as $id) {
            if ($this->choiceExists($id)) {
                $result[] = $id;
            }
        }

        return $result;
    }

    /**
     * Get first n choices
     */
    public function getChoicesList($filter) {
        // Init our dummy list - not needed if you're
        // working with a proper database
        $this->initChoices();
        ksort($_SESSION['tag_choices']);

        // Get choices list from the session; you'll use
        // something like SQL here instead

        $result = [];
        $cnt = 0;
        $limit = 10;
        $filter = mb_strtolower($filter);
        $filter_len = mb_strlen($filter);

        foreach ($_SESSION['tag_choices'] as $label => $id) {
            if ($filter_len > 0) {
                if (mb_substr(mb_strtolower($label), 0, $filter_len) !== $filter) {
                    continue;
                }
            }

            $result[$label] = $id;

            if (++$cnt >= $limit) {
                break;
            }
        }

        return $result;
    }

    /**
     * Validate whether a choice exists
     */
    protected function choiceExists($id) {
        // Init our dummy list - not needed if you're
        // working with a proper database
        $this->initChoices();

        // Check whether this choice exists in the session;
        // you'll use something like SQL here instead

        $label = array_search($id, $_SESSION['tag_choices'], true);

        return
            (
            $label === false
                ? false
                : true
            );
    }

    /**
     * Get choice label
     */
    protected function getChoiceLabel($id) {
        // Init our dummy list - not needed if you're
        // working with a proper database
        $this->initChoices();

        // Get choice from the session;
        // you'll use something like SQL here instead

        $label = array_search($id, $_SESSION['tag_choices'], true);

        return
            (
            $label === false
                ? false
                : $label
            );
    }

    /**
     * Create a new choice
     */
    protected function createChoice($label) {
        // Init our dummy list - not needed if you're
        // working with a proper database
        $this->initChoices();

        // Add choice to the session;
        // you'll use something like SQL here instead

        $id = sprintf
        (
            'choice%s (%s)',
            count($_SESSION['tag_choices']),
            $label
        );

        $_SESSION['tag_choices'][$label] = $id;

        return $id;
    }

    /**
     * Initialize a list of dummy choices
     */
    protected function initChoices() {
        if (isset($_SESSION['tag_choices'])) {
            return;
        }

        $_SESSION['tag_choices'] = [];

        for ($code = 65; $code <= 90; $code++) {
            $id = chr($code);
            $label = $id . ' tag';

            $_SESSION['tag_choices'][$label] = $id;
        }
    }

}