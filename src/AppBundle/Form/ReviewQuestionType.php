<?php

namespace AppBundle\Form;

use Slx\MetronicBundle\Form\Type\SimpleTextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class ReviewQuestionType extends AbstractType {
    private $subscribers=[];

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $reviewQuestion = $event->getData();
            $form = $event->getForm();
            if($reviewQuestion->getQuestion()) {

                $form
                    ->add('question_title', SimpleTextType::class, ['label'=>$reviewQuestion->getQuestion()->getQuestion()])
                    ->add('not_apply', null, ['required' => false, 'label' => 'not applies', 'attr'=>['class'=>'cm-not-apply-switch']])
                ;
            }
        });

        foreach($this->subscribers as $subscriber) {
            if(is_object($subscriber)) {
                $builder->addEventSubscriber($subscriber);
            }
            else {
                $builder->addEventSubscriber(new $subscriber());
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(
            [
                'data_class' => 'AppBundle\Entity\ReviewQuestion'
            ]
        );
    }

    public function getName() {
        return 'aq';
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'aq';
    }

    public function addFormSubscriber($listener) {
        $this->subscribers[] = $listener;
    }

}