<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class QuestionNumberType extends AbstractType  {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('question', TextType::class, ['label'=>'app.questions.question'])
            ->add('active', null,['label'=>'app.general.active'])
            ->add('save', SubmitType::class, array('label' => 'slx_metronic.general.save', 'attr' => array('class' => 'btn blue')))
            ->add('saveAndAdd', SubmitType::class, array('label' => 'slx_metronic.general.save_and_add', 'attr' => array('class' => 'btn blue')))

        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
           'data_class' => 'AppBundle\Entity\QuestionNumber'
        ));
    }

    public function getName() {
        return 'question';
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'question';
    }

}