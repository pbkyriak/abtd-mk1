<?php
namespace AppBundle\Transformer;

use AppBundle\Entity\ArticleAuthor;
use AppBundle\Exception\NotSupportedBibtexEntryType;
use AppBundle\Parser\BibTexParser;
use AppBundle\Entity\Article;
use AppBundle\Event\BibtexProcessedEvent;

class BibtexToArticle {

    private $transformers;
    private $dispatcher;

    public function __construct($dispatcher) {
        $this->dispatcher = $dispatcher;
        $this->transformers = [];
    }

    public function setTransformer($key, $transformer) {
        $this->transformers[$key] = $transformer;
    }

    private function hasTransformer($key) {
        return isset($this->transformers[$key]);
    }

    /**
     * @param $key string
     * @return \AppBundle\Transformer\EntryType\EntryTypeInterface
     */
    private function getTransformer($key) {
        if($this->hasTransformer($key)) {
            return $this->transformers[$key];
        }
        return null;
    }

    public function getTransformerKeys() {
        return array_keys($this->transformers);
    }
    /**
     * @param $bibtex
     * @return Artilce|null
     * @throws NotSupportedBibtexEntryType
     */
    public function toArticle($bibtex) {
        $article  = null;
        // if there is problem parsing, exception is thrown
        $entries = $this->parse($bibtex);
        // just in case check if entries were found.
        if($entries) {
            $key = strtolower($entries['TYPE']);
            if($this->hasTransformer($key)) {
                $article = $this->getTransformer($key)->process($entries);
                $article->setBibtex($bibtex);
                $event = new BibtexProcessedEvent($article);
                $this->dispatcher->dispatch(BibtexProcessedEvent::NAME, $event);
            }
            else {
                throw new NotSupportedBibtexEntryType($key);
            }
        }
        return $article;
    }

    private function parse($bibtex) {
        $out = false;
        $parser = new BibTexParser();
        $entries = $parser->parse($bibtex);
        if(is_array($entries)) {
            $out = $entries[0];
        }
        return $out;
    }

}