<?php

namespace AppBundle\Transformer;


use AppBundle\Entity\Reference;

class CrossRefOrgReferencesToEntities {

    public function do($doi, $mail) {
        $entries = $this->getDataFromCrossRef($doi, $mail);
        return $this->trans($entries);
    }

    public function trans($entries) {
        $out = [];
        foreach($entries as $entry) {
            $ref = new Reference();
            $ref->setRefKey($entry['key'])
                ->setAuthor(isset($entry['author']) ? $entry['author'] : '')
                ->setPublicationYear(isset($entry['year']) ? $entry['year'] : '')
                ->setArticleTitle(isset($entry['article-title']) ? $entry['article-title'] : '')
                ->setJournalTitle(isset($entry['journal-title']) ? $entry['journal-title'] : '')
                ->setVolumeTitle(isset($entry['volume-title']) ? $entry['volume-title'] : '')
                ->setUnstructured(isset($entry['unstructured']) ? $entry['unstructured'] : '')
                ;
            if( !empty($ref->getArticleTitle()) || !empty($ref->getJournalTitle()) || !empty($ref->getVolumeTitle()) || !empty($ref->getUnstructured()) ) {
                $out[] = $ref;
            }
        }
        return $out;
    }

    private function getDataFromCrossRef($doi, $mail) {
        $client = new \AppBundle\Client\CrossRefOrgClient();
        $data  = $client->get($doi, $mail);
        return isset($data["reference"]) ? $data["reference"] : [];
    }
}