<?php
namespace AppBundle\Transformer\EntryType;

use AppBundle\Entity\Article;
use AppBundle\Transformer\EntryType\Traits\JournalTrait;

class ArticleType extends AbstractEntryType {

    use JournalTrait;

    protected function getNewArticle() {
        $article = new Article();
        $article->setArticleType('article');
        return $article;
    }
}