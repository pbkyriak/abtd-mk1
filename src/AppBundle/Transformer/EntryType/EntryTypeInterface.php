<?php

namespace AppBundle\Transformer\EntryType;


interface EntryTypeInterface {

    public function process(array $entries);

}