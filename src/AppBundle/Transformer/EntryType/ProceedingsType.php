<?php

namespace AppBundle\Transformer\EntryType;


use AppBundle\Entity\Article;
use AppBundle\Transformer\EntryType\Traits\JournalTrait;
use AppBundle\Transformer\EntryType\Traits\PublisherTrait;

class ProceedingsType extends AbstractEntryType {
    use JournalTrait;
    use PublisherTrait;

    protected function getNewArticle() {
        $article = new Article();
        $article->setArticleType('inproceedings');
        return $article;
    }

}