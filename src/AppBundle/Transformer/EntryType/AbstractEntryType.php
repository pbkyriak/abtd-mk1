<?php

namespace AppBundle\Transformer\EntryType;


use AppBundle\Entity\Article;
use AppBundle\Matcher\MatcherManager;
use AppBundle\Transformer\EntryType\Traits\AuthorsTrait;
use AppBundle\Transformer\EntryType\Traits\DateTrait;
use AppBundle\Transformer\EntryType\Traits\DoiTrait;
use AppBundle\Transformer\EntryType\Traits\KeywordsTrait;
use AppBundle\Transformer\EntryType\Traits\TitleTrait;
use AppBundle\Transformer\EntryType\Traits\UrlTrait;

abstract class AbstractEntryType implements EntryTypeInterface {

    use TitleTrait;
    use DateTrait;
    use DoiTrait;
    use UrlTrait;
    use AuthorsTrait;
    use KeywordsTrait;

    protected $matcherManager;

    public function __construct(MatcherManager $matcherManager) {
        $this->matcherManager = $matcherManager;
    }

    /**
     * @param $entries
     * @return Article
     */
    public function process(array $entries) {
        $article = $this->getNewArticle();
        $article->setArticleType($entries['TYPE']);

        foreach($entries as $key => $entry) {
            $methods = [
                'set'.ucfirst(strtolower($key)),
                'add'.ucfirst(strtolower($key)),
                'add'.ucfirst(strtolower($key)).'s',
            ];
//            printf("key=%s<br/>", $key  );
//            print_r($methods);
            foreach($methods as $method) {
                if (method_exists($this, $method)) {
                    //printf("calling :%s<br />",$method );
                    $this->$method($entry, $article);
                    break;
                }
            }
        }
//            die(1);
        return $article;
    }

    abstract protected function getNewArticle();
}