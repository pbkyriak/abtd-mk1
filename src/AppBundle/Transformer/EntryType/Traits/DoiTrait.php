<?php

namespace AppBundle\Transformer\EntryType\Traits;

use AppBundle\Entity\Article;

trait DoiTrait {
    protected function setDoi($entry, Article $article) {
        $article->setDoi(!empty($entry) ? $entry : '');
    }

}