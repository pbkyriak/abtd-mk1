<?php

namespace AppBundle\Transformer\EntryType\Traits;

use AppBundle\Entity\Article;

trait KeywordsTrait {
    protected function addKeywords($keywords, Article $article) {
        foreach($keywords as $k) {
            $keyword = $journal = $this->matcherManager->get('keyword')->matchOrCreate(['title'=>$k]);
            $article->addKeyword($keyword);
        }
    }

}