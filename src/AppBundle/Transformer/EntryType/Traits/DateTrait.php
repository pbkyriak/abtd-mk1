<?php

namespace AppBundle\Transformer\EntryType\Traits;

use AppBundle\Entity\Article;

trait DateTrait {

    protected function setYear($entry, Article $article) {
        $this->setDate($entry, $article);
    }
    protected function setDate($entry, Article $article) {
        $m = [];
        if( preg_match("/^(\d{4})/", $entry, $m) ) {
            $entry = $m[1];
        }

        $article->setPublishDate($entry);
    }

}