<?php

namespace AppBundle\Transformer\EntryType\Traits;

use AppBundle\Entity\Article;

trait PublisherTrait {

    protected function setPublisher($entry, Article $article) {
        $title = trim($entry);
        if(!empty($title)) {
            $publisher = $journal = $this->matcherManager->get('publisher')->matchOrCreate(['title'=>$title]);
            $article->setPublisher($publisher);
        }
    }

}