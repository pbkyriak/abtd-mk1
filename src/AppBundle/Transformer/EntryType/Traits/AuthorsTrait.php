<?php

namespace AppBundle\Transformer\EntryType\Traits;

use AppBundle\Entity\Article;
use AppBundle\Entity\ArticleAuthor;

trait AuthorsTrait {

    protected function addAuthors($entries, Article $article) {
        if(empty($entries)) {
            return false;
        }
        $aorder = 1;
        foreach($entries as $auth) {
            $aff = [
                !empty($auth['von']) ? $auth['von'] : '',
                !empty($auth['jr']) ? $auth['jr'] : '',
            ];
            $author = $this->matcherManager->get('author')->matchOrCreate(
                [
                    'family'=>$auth['last'],
                    'given'=>$auth['first'],
                    'affiliation'=>implode(',', $aff )
                ]
            );
            $articleAuthor = new ArticleAuthor();
            $articleAuthor->setArticle($article);
            $articleAuthor->setAuthor($author);
            $articleAuthor->setAorder($aorder++);
            $article->addAuthor($articleAuthor);
        }
    }
}