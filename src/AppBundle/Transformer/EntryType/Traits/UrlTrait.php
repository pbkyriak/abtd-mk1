<?php

namespace AppBundle\Transformer\EntryType\Traits;

use AppBundle\Entity\Article;

trait UrlTrait {
    protected function setUrl($entry, Article $article) {
        $article->setUrl(!empty($entry) ? $entry : '');
    }

}