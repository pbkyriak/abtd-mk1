<?php

namespace AppBundle\Transformer\EntryType\Traits;

use AppBundle\Entity\Article;

trait TitleTrait {
    protected function setTitle($entry, Article $article) {
        $article->setTitle($entry);
    }

}