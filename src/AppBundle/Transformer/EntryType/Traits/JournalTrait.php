<?php

namespace AppBundle\Transformer\EntryType\Traits;

use AppBundle\Entity\Article;

trait JournalTrait {
    protected function setBooktitle($entry, Article $article) {
        $this->setJournal($entry,$article);
    }
    protected function setJournaltitle($entry, Article $article) {
        $this->setJournal($entry,$article);
    }

    protected function setJournal($entry, Article $article) {
        $title = trim($entry);
        if(!empty($title)) {
            $journal = $this->matcherManager->get('journal')->matchOrCreate(['title'=>$title]);
            $article->setJournal($journal);
        }
    }

}