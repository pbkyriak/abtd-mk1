<?php

namespace AppBundle\Transformer\EntryType;

use AppBundle\Entity\Article;
use AppBundle\Transformer\EntryType\Traits\PublisherTrait;

class BookType extends AbstractEntryType {
    use PublisherTrait;

    protected function getNewArticle() {
        $article = new Article();
        $article->setArticleType('book');
        return $article;
    }

}