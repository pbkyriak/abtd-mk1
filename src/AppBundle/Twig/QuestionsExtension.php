<?php
namespace AppBundle\Twig;

use AppBundle\Entity\ReviewQuestion;
use AppBundle\Helper\QuestionTypes;
use AppBundle\Helper\QuestionTypesManager;

class QuestionsExtension extends \Twig_Extension {

    private $qtManager;
    private $templating;

    public function __construct(QuestionTypesManager $qtManager) {
        $this->qtManager = $qtManager;
        //$this->templating = $templating;
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('qtypeToText', array($this, 'qtypeToText')),
        );
    }

    public function getFunctions() {
        return [
            new \Twig_SimpleFunction('questionTypes', [$this, 'getQuestionTypes']),
            new \Twig_SimpleFunction('getIfMultipleQuestionAnswers', [$this, 'getIfMultipleQuestionAnswers']),
            new \Twig_SimpleFunction('renderQuestionAnswer', [$this, 'renderQuestionAnswer'],['needs_environment' => true,'is_safe' => ['html']]),
        ];
    }

    public function qtypeToText($obj) {
        $fqn = get_class($obj);
        $parts = explode("\\", $fqn);
        $class= strtolower(array_pop($parts));
        return str_replace('question', 'app.questions.',$class);
    }

    public function getQuestionTypes() {
        return $this->qtManager->getQuestionTypeDescriminators();
    }

    public function renderQuestionAnswer(\Twig_Environment $environment,ReviewQuestion $rq) {
        return $environment->render(
            $this->qtManager->getTypeTemplate($rq->getQuestion()),
            array( 'rq' => $rq )
        );

    }

    /**
     * if is multiple answer question returns array with id->answer pairs
     *
     */
    public function getIfMultipleQuestionAnswers(ReviewQuestion $rq) {
        $out = [];
        $qtype = $this->qtManager->entityToDiscr($rq->getQuestion());
        if($qtype=='m') {
            foreach($rq->getAnswer() as $qa) {
                $out[] = [
                    'id'=>$qa->getId(),
                    'qid'=> [sprintf("fltreview[question%s][]",$rq->getQuestion()->getId()) => $qa->getId()],
                    'title'=>$qa->getTitle()
                ];
            }
        }
        return $out;
    }

    public function getName() {
        return 'questions_extension';
    }
}