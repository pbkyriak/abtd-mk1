<?php

namespace AppBundle\Parser\ParserProcessor;

use RenanBr\BibTexParser\Exception\ProcessorException;
use RenanBr\BibTexParser\Processor\TagCoverageTrait;

class LatexResolverProcessor {

    use TagCoverageTrait;

    /**
     * @param array $entry
     * @return array
     */
    public function __invoke(array $entry) {
        $covered = $this->getCoveredTags(array_keys($entry));
        foreach ($covered as $tag) {
            // Translate string
            if (is_string($entry[$tag])) {
                $entry[$tag] = str_replace(['{','}'],'',$this->decode($entry[$tag]));
                continue;
            }

            // Translate array
            if (is_array($entry[$tag])) {
                array_walk_recursive($entry[$tag], function (&$text) {
                    if (is_string($text)) {
                        $text = str_replace(['{','}'],'',$this->decode($text));
                    }
                });
            }
        }

        return $entry;
    }

    /**
     * @param string $entry
     * @param mixed  $text
     * @return string
     */
    private function decode($text) {
        $out = preg_replace("/{.+{(\S+)}}/","$1",$text);
        return $out;
    }
}