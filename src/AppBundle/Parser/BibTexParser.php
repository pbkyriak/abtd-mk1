<?php
namespace AppBundle\Parser;

use AppBundle\Parser\ParserProcessor\LatexResolverProcessor;
use RenanBr\BibTexParser\Parser;
use RenanBr\BibTexParser\Listener;
use RenanBr\BibTexParser\Processor\TagNameCaseProcessor;
use RenanBr\BibTexParser\Processor\NamesProcessor;
use RenanBr\BibTexParser\Processor\KeywordsProcessor;


class BibTexParser {

    public function parse($bibtex) {
        $parser = new Parser();          // Create a Parser
        $listener = new Listener();      // Create and configure a Listener
        $listener->addProcessor(new LatexResolverProcessor());
        $listener->addProcessor(new TagNameCaseProcessor(CASE_UPPER)); // or CASE_LOWER
        $listener->addProcessor(new NamesProcessor());
        $listener->addProcessor(new KeywordsProcessor());
        $parser->addListener($listener); // Attach the Listener to the Parser
        //
        $parser->parseString($bibtex);   // or parseFile('/path/to/file.bib')
        $entries = $listener->export();  // Get processed data from the Listener
        return $entries;
    }

}