<?php

namespace AppBundle\Menu;

use Slx\MetronicBundle\Menu\AbstractMenuBuilder;

class MenuBuilder extends AbstractMenuBuilder {

    private $curi = '';
    private $isAdminArea = false;

    public function setRequestStack($requestStack) {
        $this->curi = $requestStack->getCurrentRequest()->getUri();
        if(strpos($this->curi, '/admin')!==false) {
            $this->isAdminArea = true;
        }
    }

    public function createMainMenu(array $options) {
        //------------------------
        $isAdmin = $this->securityContext->isGranted(array('ROLE_ADMIN'));
        $isSuperAdmin = $this->securityContext->isGranted(array('ROLE_SUPER_ADMIN'));
        //-----------------------
        $menu = $this->factory->createItem('root');
        $menu->addChild('slx_metronic.general.home', array('route' => 'slx_metronic_homepage'))
             ->setAttribute('icon', 'fa-home');
        $ent = $menu->addChild('Articles')->setAttribute('icon', 'fa-stack-overflow');
        $ent->addChild('app.questions.questions', array('route' => 'questions'));
        if ($isAdmin) {
            $org = $menu->addChild('slx_metronic.menu.settings')->setAttribute('icon', 'fa-cogs');
            $org->addChild('users.user.users', array('route' => 'users_manage'))->setAttribute('icon', 'fa-users');
        }

        if ($this->currentItemRouteName) {
            $this->setCurrentMenuItem($menu, $this->currentItemRouteName);
        }
        return $menu;
    }

    public function createPublicMenu(array $options) {
        $menu = $this->factory->createItem('root');
        $menu->addChild('slx_metronic.general.home', array('route' => 'home_page'))
             ->setAttribute('icon', 'fa-home');
        $ent = $menu->addChild('Articles', array('route' => 'articles'))
                    ->setAttribute('icon', 'fa-stack-overflow');
        $ent2 = $menu->addChild('Statistics', array('route' => 'basic_stats'))
                    ->setAttribute('icon', 'fa-bar-chart');

        if ($this->currentItemRouteName) {
            $this->setCurrentMenuItem($menu, $this->currentItemRouteName);
        }
        return $menu;
    }

    public function createUserMenu(array $options) {
        $isAdministrator = $this->securityContext->isGranted(array('ROLE_ADMIN'));
        $menu = $this->factory->createItem('root');

        if ($this->securityContext->isGranted(array('ROLE_EDITOR'))) {
            $menu->addChild('app.article.add_article', array('route' => 'article_create'))
                 ->setAttribute('icon', 'fa-plus');
            $menu->addChild('app.article.my_drafts', array('route' => 'articles', 'routeParameters'=>['my_drafts'=>1]))
                 ->setAttribute('icon', 'fa-stack-overflow');
        }
        $menu->addChild('users.user.change_password', array('route' => 'fos_user_change_password'))
             ->setAttribute('icon', 'icon-key');
        $menu->addChild('users.profile.edit', array('route' => 'fos_user_profile_show'))
             ->setAttribute('icon', 'fa-user');
        if ($isAdministrator) {
            if(!$this->isAdminArea) {
                $menu->addChild('Administer', array('route' => 'slx_metronic_homepage'))
                     ->setAttribute('icon', 'fa-cogs')
                     ->setAttribute('divider_prepend', true);
            }
            else {
            $menu->addChild('View site', array('route' => 'home_page'))
                 ->setAttribute('icon', 'fa-home')->setAttribute('divider_prepend', true);
            }
        }
        $menu->addChild('logout', array('route' => 'fos_user_security_logout'))
             ->setAttribute('icon', 'icon-key')
             ->setAttribute('divider_prepend', true);

        return $menu;
    }
}
