<?php

namespace AppBundle\ArticleFilters;

use Doctrine\ORM\QueryBuilder;

interface ArticleFilterInterface {

    public function applyFilter(QueryBuilder $qb, $queryParams);

}