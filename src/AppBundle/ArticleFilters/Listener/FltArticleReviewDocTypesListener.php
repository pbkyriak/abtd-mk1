<?php

namespace AppBundle\ArticleFilters\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Form event listener. Adds Doc Types filter field to the form.
 * @package AppBundle\ArticleFilters\Listener
 */
class FltArticleReviewDocTypesListener implements EventSubscriberInterface {

    private $articleTypes;

    public function __construct($trans) {
        $this->articleTypes = $trans->getTransformerKeys();
    }

    public static function getSubscribedEvents() {
        return array(
            FormEvents::PRE_SET_DATA => 'onPreSetData',
        );
    }

    public function onPreSetData(FormEvent $event) {
        $flt = $event->getData();
        $form = $event->getForm();

        $c = [];
        foreach($this->articleTypes as $t) {
            $c['app.article.type_'.$t] = $t;
        }

        $form->add(
            'article_type',
            ChoiceType::class,
            [
                'label' => 'app.article.article_type',
                'translation_domain' => 'messages',
                'required' => false,
                'choices' => $c,
                'multiple' => false,
                'expanded' => true,
                'placeholder' => 'Any'
            ]
        );

    }

}