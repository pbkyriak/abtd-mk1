<?php

namespace AppBundle\ArticleFilters\Listener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\RequestStack;
use Slx\LockedEntityTypeBundle\Form\Type\LockedEntityType;

/**
 * Add here any extra fields that are coming from links like author, keywords etc
 *
 * @package AppBundle\ArticleFilters\Listener
 */
class FltArticleReviewRequestParamsListener implements EventSubscriberInterface {

    private $queryParams;

    public function __construct(RequestStack $requestStack) {
        $this->queryParams = $requestStack->getCurrentRequest()->query->all();
    }

    public static function getSubscribedEvents() {
        return array(
            FormEvents::PRE_SET_DATA => 'onPreSetData',
        );
    }

    public function onPreSetData(FormEvent $event) {
        $flt = $event->getData();
        $form = $event->getForm();
        $this->addIfNeededKeywordField($flt,$form);
        $this->addIfNeededAuthorField($flt,$form);
    }

    private function addIfNeededKeywordField($flt, $form) {
        if(!empty($this->queryParams['fltreview']['keyword_id'])) {
            $form->add(
                'keyword_id',
                LockedEntityType::class,
                [
                    'class' => 'AppBundle:Keyword',
                    'label' => 'Keyword',
                    'choice_value' => 'id',
                    'choice_label' => function ($entity) {
                        return ucfirst($entity->getTitle());
                    },
                    'attr' => [
                        'class' => '213',
                    ],
                ]
            );
        }
    }

    private function addIfNeededAuthorField($flt, $form) {
        if(!empty($this->queryParams['fltreview']['author_id'])) {
            $form->add(
                'author_id',
                LockedEntityType::class,
                [
                    'class' => 'AppBundle:Author',
                    'label' => 'Author',
                    'choice_value' => 'id',
                    'choice_label' => function ($entity) {
                        return $entity->getFamily().' '. $entity->getGiven();
                    },
                ]
            );
        }
    }

}