<?php

namespace AppBundle\ArticleFilters\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Form event listener. Adds Questions filter field to the form.
 * @package AppBundle\ArticleFilters\Listener
 */
class FltArticleReviewQuestionsListener implements EventSubscriberInterface {

    public static function getSubscribedEvents() {
        return array(
            FormEvents::PRE_SET_DATA => 'onPreSetData',
        );
    }

    public function onPreSetData(FormEvent $event) {
        $flt = $event->getData();
        $form = $event->getForm();

        foreach ($flt as $qid => $question) {
            $c = unserialize($question['choices']);
            $form
                ->add(
                    $qid,
                    ChoiceType::class,
                    [
                        'label' => $question['label'],
                        'required' => false,
                        'choices' => $c,
                        'multiple' => true,
                        'expanded' => true,
                    ]
                );
        }
    }

}