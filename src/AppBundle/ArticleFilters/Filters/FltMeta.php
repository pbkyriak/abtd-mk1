<?php

namespace AppBundle\ArticleFilters\Filters;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use AppBundle\ArticleFilters\ArticleFilterInterface;

class FltMeta implements ArticleFilterInterface {
    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    /**
     * Adds to queryBuilder returned from FltQuestions extra filters
     * from request parameters.
     *
     * @param QueryBuilder $qb
     * @param string       $queryParams
     */
    public function applyFilter(QueryBuilder $qb, $queryParams) {
        $this->keywordFilter($qb, $queryParams);
        $this->authorFilter($qb, $queryParams);
        $this->articleTypeFilter($qb, $queryParams);
    }

    private function keywordFilter(QueryBuilder $qb, $queryParams) {
        if(isset($queryParams['keyword_id'])) {
            if(!empty($queryParams['keyword_id'])) {
                $qb->leftJoin("a.keywords", 'k')
                   ->andWhere("k.id=:kid")
                   ->setParameter("kid", $queryParams['keyword_id']);
            }
        }
    }

    private function authorFilter(QueryBuilder $qb, $queryParams) {
        if(isset($queryParams['author_id'])) {
            if (!empty($queryParams['author_id'])) {
                $qb->leftJoin("a.authors", 'auth')
                   ->andWhere("auth.author=:authid")
                   ->setParameter("authid", $queryParams['author_id']);
            }
        }
    }

    private function articleTypeFilter($qb, $questions) {
        foreach($questions as $qidx => $answers) {
            if ($qidx=='article_type') {
                if(!empty($answers)) {
                    $qb->andWhere("a.article_type=:at")
                       ->setParameter(':at', $answers);
                }
            }
        }
    }

}