<?php

namespace AppBundle\ArticleFilters\Filters;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use AppBundle\ArticleFilters\ArticleFilterInterface;

class FltQuestions implements ArticleFilterInterface {

    private $em;
    private $questions;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
        $this->questions = $em->getRepository("AppBundle:Question")->getFilterQuestions();
    }

    public function getQuestions() {
        $out = [];
        foreach($this->questions as $question) {
            $q = [
                'id' => $question->getId(),
                'label' => $question->getQuestion(),
                'choices' => [],
            ];
            foreach ($question->getAnswers() as $answer) {
                $q['choices'][$answer->getTitle()] = $answer->getId();
            }
            $q['choices'] = serialize($q['choices']);
            $out['question'.$question->getId()] = $q;
        }
        return $out;
    }

    public function applyFilter(QueryBuilder $qb, $queryParams) {
        if(!empty($queryParams)) {
            $this->addQuestionsFilters($qb,$queryParams);
        }
        return $qb;
    }

    /*
    public function buildFilterQuery($params) {
        $qb = $this->buildFilterQueryBuilder($params);
        return $qb->getQuery();
    }
    */

    /**
     * Adds fulltext search for each question's answers user selected.
     * For example:
     * SELECT * FROM article_review WHERE MATCH(srch_answer_idx) AGAINST('+(_3_2 _3_1) +_4_6' IN BOOLEAN MODE);
     * FullText tutorial: http://www.mysqltutorial.org/mysql-boolean-text-searches.aspx
     *
     * @param $qb
     * @param $questions
     */
    private function addQuestionsFilters($qb, $questions) {
        $aga = [];
        foreach($questions as $qidx => $answers) {
            if(strpos($qidx, 'question')===false) {
                continue;
            }
            $qId = (int)str_replace('question','',$qidx);
            if(count($answers)) {
                $laga = [];
                foreach($answers as $aid) {
                    $laga[] = sprintf("_%s_%s", $qId, $aid);
                }
                $aga[] = sprintf("+(%s)", implode(' ', $laga));

            }
        }
        if(count($aga)) {
            $qb->andWhere("MATCH_AGAINST(r.srch_answer_idx, :searchterm 'IN BOOLEAN MODE')>0")
                ->setParameter("searchterm", implode(' ', $aga));
        }
    }

}