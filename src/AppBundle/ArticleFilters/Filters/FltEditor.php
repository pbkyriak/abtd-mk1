<?php

namespace AppBundle\ArticleFilters\Filters;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use AppBundle\Entity\Article;
use AppBundle\ArticleFilters\ArticleFilterInterface;

class FltEditor implements ArticleFilterInterface {
    private $em;
    private $tokenStorage;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Adds to queryBuilder returned from FltQuestions extra filters
     * from request parameters.
     *
     * @param QueryBuilder $qb
     * @param              $queryParams
     */
    public function applyFilter(QueryBuilder $qb, $queryParams) {
        $this->statusFilter($qb, $queryParams);
    }

    private function statusFilter(QueryBuilder $qb, $queryParams) {
        $user = $this->getUser();
        if(isset($queryParams['my_drafts']) && is_object($user)) {
            $qb
                ->andWhere("a.status=:stat")
                ->andWhere("a.owner=:usr")
                ->setParameter("stat", Article::STATUS_DRAFT)
                ->setParameter("usr", $user)
            ;
        }
        else {
            $qb
                ->andWhere("a.status=:stat")
                ->setParameter("stat", Article::STATUS_PUBLISHED)
                ;
        }
    }

    private function getUser() {
        $user = null;

        if($this->tokenStorage->getToken()) {
            $user = $this->tokenStorage->getToken()->getUser();
            if(!is_object($user)) {
                $user = null;
            }
        }
        return $user;

    }

}