<?php

namespace AppBundle\ArticleFilters;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class ArticleFilters {

    private $filters;
    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
        $this->filters = [];
    }

    public function addFilter($type, ArticleFilterInterface $filter) {
        $this->filters[$type] = $filter;
    }

    public function getFilterTypes() {
        return array_keys($this->filters);
    }

    public function getFilter($type) {
        if(isset($this->filters[$type])) {
            return $this->filters[$type];
        }
        return null;
    }

    public function applyFilters($formData, $queryParams) {
        $tmp = isset($queryParams['fltreview']) ? $queryParams['fltreview'] : [];
        unset($queryParams['fltreview']);
        $params = array_merge($queryParams, $tmp);

        $qb = $this->em
            ->createQueryBuilder()
            ->select('a')
            ->from('AppBundle:Article', 'a')
            ->leftJoin('a.review', 'r')
        ;

        foreach($this->filters as $filter) {
            $filter->applyFilter($qb, $params);
        }

        $qb->orderBy("a.created_at", "DESC");
        return $qb;
    }
}