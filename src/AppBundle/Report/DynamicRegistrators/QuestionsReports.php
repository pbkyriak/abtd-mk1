<?php

namespace AppBundle\Report\DynamicRegistrators;

use AppBundle\Entity\QuestionMultiple;
use AppBundle\Report\DynamicRegistratorInterface;
use AppBundle\Report\QuestionReport;
use AppBundle\Report\ReportFactory;
use Doctrine\ORM\EntityManagerInterface;

class QuestionsReports implements DynamicRegistratorInterface {

    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function addReports(ReportFactory $factory) {
        $questions = $this->em->getRepository("AppBundle:Question")->getActiveMSQuestions();

        foreach($questions as $q) {
            $type =
            $params['title'] = $q->getQuestion();
            $params['options'] = [
                'chart_type' => ($q instanceof QuestionMultiple) ? 'bars' : 'yn',
                'question_id' => $q->getId(),
            ];
            $rpt = new QuestionReport($this->em);
            $factory->addReport('q'.$q->getId(), $rpt, $params);
        }

    }

}