<?php

namespace AppBundle\Report;


interface DynamicRegistratorInterface {

    public function addReports(ReportFactory $factory);

}