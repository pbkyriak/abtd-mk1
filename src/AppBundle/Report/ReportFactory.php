<?php

namespace AppBundle\Report;

use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportFactory {

    private $reportDefinitions = [];

    private $resolver;

    public function __construct() {
        $this->resolver = new OptionsResolver();
        $this->resolver->setDefined('title');
        $this->resolver->setDefault('options', []);
    }

    public function getReportList() {
        $out = [];
        foreach ($this->reportDefinitions as $k => $r) {
            $out[$k] = $r['t'];
        }
        return $out;
    }

    public function getReport($id) {
        if(isset($id)) {
            $def = $this->reportDefinitions[$id];
            $def['o']->run($def['p']);
            return $def['o'];
        }
        else {
            return null;
        }
    }

    public function addReport($id, $obj, $params) {
        $params = $this->resolver->resolve($params);
        $title = !empty($params['title']) ? $params['title'] : $obj::getTitle();
        $this->reportDefinitions[$id] = ['o'=>$obj, 'p'=>$params['options'], 't'=>$title];
    }

    public function addDynReports($register) {
        $register->addReports($this);
    }

}