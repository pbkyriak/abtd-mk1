<?php

namespace AppBundle\Report;


class ReportConstants {
    const CHART_BARS = 'bars';
    const CHART_BARS_STACKED = 'bars-stacked';
    const CHART_PIE = 'pie';
}