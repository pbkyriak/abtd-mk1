<?php

namespace AppBundle\Report\Reports;


use AppBundle\Report\ReportConstants;
use AppBundle\Report\ReportInterface;
use Doctrine\ORM\EntityManagerInterface;

class ShareByTypeRpt implements ReportInterface {

    private $em;
    private $trans;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function setTrans($trans) {
        $this->trans = $trans;
    }

    public static function getTitle() {
        return 'Share by Type';
    }

    public function getChartType() {
        return ReportConstants::CHART_PIE;
    }

    public function getData() {
        return $this->data;
    }

    public function run($params) {
        $articleTypes = $this->trans->getTransformerKeys();
        $stmt = $this->em->getConnection()->executeQuery("SELECT article_type, COUNT(id) cnt FROM article WHERE STATUS='P' GROUP BY article_type");
        $results = $stmt->fetchAll(\PDO::FETCH_KEY_PAIR);
        $out = [];
        $total = array_sum($results);
        foreach ($articleTypes as $type) {
            $out[$type]= isset($results[$type]) ? $results[$type] : 0;
            $out[$type] = round(100*$out[$type]/$total,0);
        }
        $this->data = $out;
    }

}