<?php

namespace AppBundle\Report;


interface ReportInterface {

    public static function getTitle();
    public function getChartType();
    public function getData();
    public function run($params);

}