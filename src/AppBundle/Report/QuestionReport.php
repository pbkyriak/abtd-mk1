<?php

namespace AppBundle\Report;

use Doctrine\ORM\EntityManagerInterface;

class QuestionReport implements ReportInterface {
    private $chartType = '';
    private $questionId = null;
    private $data;
    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public static function getTitle() {
        return 'Q';
    }

    public function getChartType() {
        return $this->chartType;
    }

    public function getData() {
        return $this->data;
    }

    public function run($params) {
        $this->chartType = $params['chart_type'];
        $this->questionId = $params['question_id'];

        $question = $this->em->getRepository("AppBundle:Question")->find($this->questionId);

        foreach ($question->getAnswers() as $a) {
            $this->data[$a->getTitle()] = $a->getArticleCount();
        }
        return $this;
    }

}

/*
 * I want to dynamically create reports from question?
 * I want to manually create complex reports
 * I want a common way to handle both
 */