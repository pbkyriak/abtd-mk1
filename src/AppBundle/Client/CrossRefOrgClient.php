<?php

namespace AppBundle\Client;

use GuzzleHttp\Client;

class CrossRefOrgClient {

    public function get($doi, $mail) {
        $out =false;
        $client = new Client([
                                 'timeout' => 55.0,
                                 'http_errors' => false,
                             ]);
//        https://api.crossref.org/works/10.1145/375360.375365?mailto=panos@salix.gr
        $headers = [
            'Accept' => 'application/json',
            'Accept-Encoding' => 'gzip,deflate',
            'User-Agent' => 'crossref-dois/0.1 (+https://github.com/hubgit/crossref-dois/)',
        ];
        $params = sprintf("%s?mailto=%s", $doi, $mail);

        try {
            $response = $client->get('https://api.crossref.org/works/' . $params, [
                //'debug' => true,
                'connect_timeout' => 10,
                'timeout' => 120,
                'headers' => $headers,
            ]);

            if ($response->getStatusCode() == 200) {
                $body = (string)$response->getBody();
            }
            elseif ($response->getStatusCode() == 404) {
                $body = null;
            }
        }
        catch (\Exception $ex) {
            $body = null;
        }
        if($body) {
            try {
                $out = json_decode($body, true);
                if($out['status']!=='ok') {
                    $out = false;
                }
                else {
                    $out = $out['message'];
                }
            }
            catch(\Exception $ex) {
                $out = false;
            }
        }
        return $out;
    }
}