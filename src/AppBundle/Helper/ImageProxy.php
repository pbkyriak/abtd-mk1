<?php
/**
 * @copyright 2018 Kartpay.com.
 * @author Panos <panos@kartpay.com>
 * Date: 16/3/2018
 * Time: 8:39 μμ
 */

namespace AppBundle\Helper;

use Doctrine\ORM\EntityManagerInterface;

class ImageProxy {
    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function register($imageLink) {
        $conn = $this->em->getConnection();
        $links = $conn->fetchAll("select * from image_links where url=:url", ['url'=>$imageLink]);
        $out = false;
        if($links) {
            $link = reset($links);
            if($link['id']) {
                $out = $link['id'];
            }
        }
        else {
            $conn->insert('image_links', ['url'=>$imageLink]);
            $out = $conn->lastInsertId();
        }
        return $out;
    }

    public function getSource($id) {
        $conn = $this->em->getConnection();
        $links = $conn->fetchAll("select * from image_links where id=:dd", ['dd'=>$id]);
        $out = false;
        if($links) {
            $link = reset($links);
            $out = $link['url'];
        }
        return $out;
    }
}
