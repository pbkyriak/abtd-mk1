<?php

namespace AppBundle\Helper;


use AppBundle\Exception\NotSupportedQuestionTypeException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class QuestionTypesManager {

    private $templates=[];
    private $entityClasses=[];
    private $formClasses=[];
    private $reviewQuestionFieldListeners=[];

    /**
     * Add a new question type
     *
     * @param $type
     * @param $entityClass
     * @param $formClass
     */
    public function addType($type, $entityClass, $formClass) {
        $this->entityClasses[$type] = $entityClass;
        $this->formClasses[$type] = $formClass;
    }

    /**
     * replace question type renderer template
     *
     * @param $type
     * @param $template
     */
    public function setTypeTemplate($type, $template) {
        $this->templates[$type] = $template;
    }

    /**
     * Get question type renderer template
     *
     * @param $type
     * @return mixed
     */
    public function getTypeTemplate($type) {
        if( is_object($type)) {
           $type = $this->entityToDiscr($type);
        }
        if(isset($this->templates[$type])) {
            return $this->templates[$type];
        }
        throw new NotFoundResourceException(sprintf("No template defined for question type '%s'", $type));
    }

    /**
     * Replace question type entity class
     *
     * @param $type
     * @param $entityClass
     * @throws NotSupportedQuestionTypeException
     */
    public function setTypeEntityClass($type, $entityClass) {
        if(isset($this->entityClasses[$type])) {
            $this->entityClasses[$type] = $entityClass;
        }
        throw new NotSupportedQuestionTypeException($type);
    }

    /**
     * Replace question type form type class
     *
     * @param $type
     * @param $formClass
     * @throws NotSupportedQuestionTypeException
     */
    public function setTypeFormClass($type, $formClass) {
        if(isset($this->formClasses[$type])) {
            $this->formClasses[$type] = $formClass;
        }
        throw new NotSupportedQuestionTypeException($type);
    }

    /*
     * Dummy operation, listeners are added to form via
     * compilers pass to ReviewQuestionType directly.
     *
     */
    public function addReviewQuestionFieldListener($fqn) {
        $this->reviewQuestionFieldListeners[] = $fqn;
    }

    /**
     * Return question types discriminators
     *
     * @return array
     */
    public function getQuestionTypeDescriminators() {
        return array_keys($this->entityClasses);
    }

    /**
     * Given entity FQN or entity object returns discriminator char
     *
     * @param $entity
     * @return false|int|string
     * @throws NotSupportedQuestionTypeException
     */
    public function entityToDiscr($entity) {
        if(is_object($entity)) {
            $eclass = get_class($entity);
        }
        else {
            $eclass = $entity;
        }
        $discr = array_search($eclass, $this->entityClasses);
        if(!$discr) {
            throw new NotSupportedQuestionTypeException($eclass);
        }
        return $discr;
    }

    /**
     * Given discriminator or entity returns form type FQN
     *
     * @param $discr
     * @return mixed|string
     * @throws NotSupportedQuestionTypeException
     */
    public function discrToFormType($discr) {
        $formType = '';
        if(is_object($discr)) {
            $srch = $this->entityToDiscr($discr);
        }
        else {  // if longer than 1 char, then should be object or FQN
            $srch = strtolower($discr);
        }
        if(isset($this->formClasses[$srch])) {
            $formType = $this->formClasses[$srch];
        }
        else {
            throw new NotSupportedQuestionTypeException($srch);
        }
        return $formType;
    }

    /**
     * Given discriminator returns entity FQN
     *
     * @param $discr
     * @return mixed
     * @throws NotSupportedQuestionTypeException
     */
    public function discrToEntityType($discr) {
        if(isset($this->entityClasses[$discr])) {
            return $this->entityClasses[$discr];
        }
        throw new NotSupportedQuestionTypeException($discr);
    }

}