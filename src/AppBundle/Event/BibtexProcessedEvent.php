<?php

namespace AppBundle\Event;

use AppBundle\Entity\Article;
use Symfony\Component\EventDispatcher\Event;

class BibtexProcessedEvent extends Event {

    const NAME = 'bibtex.parsed';

    private $article;

    public function __construct(Article $article) {
        $this->article = $article;
    }

    /**
     * @return Article
     */
    public function getArticle() {
        return $this->article;
    }
}