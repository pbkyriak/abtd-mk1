<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
class Journal {
    use TimestampableTrait;

    private $id;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=2)
     */
    private $title;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $articles;

    public function __construct() {
        $this->articles = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Journal
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Journal
     */
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    public function addArticle(Article $article) {
        $this->articles[] = $article;
        return $this;
    }

    public function removeArticle(Article $article) {
        $this->articles->removeElement($article);
        return $this;
    }

}