<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Keyword {
    private $id;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Keyword
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Keyword
     */
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }


}