<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimestampableTrait;

class ArticleComment {

    use TimestampableTrait;

    private $id;
    /**
     * @var Article
     */
    private $article;
    /**
     * @var User
     */
    private $commenter;
    /**
     * @var string
     */
    private $comment_text;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getArticle() {
        return $this->article;
    }

    public function setArticle(Article $article) {
        $this->article = $article;
    }

    /**
     * @return User
     */
    public function getCommenter() {
        return $this->commenter;
    }

    /**
     * @param User $commenter
     * @return $this
     */
    public function setCommenter(User $commenter) {
        $this->commenter = $commenter;
        return $this;
    }

    public function getCommentText() {
        return $this->comment_text;
    }

    public function setCommentText($text) {
        $this->comment_text = $text;
        return $this;
    }

}