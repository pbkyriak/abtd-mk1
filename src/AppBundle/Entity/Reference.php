<?php

namespace AppBundle\Entity;


class Reference {

    private $id;
    private $ref_key;
    private $author;
    private $publication_year;


    private $article_title;
    private $journal_title;
    private $volume_title;
    private $unstructured;
    private $article;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Reference
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * @param mixed $author
     * @return Reference
     */
    public function setAuthor($author) {
        $this->author = $author;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArticleTitle() {
        return $this->article_title;
    }

    /**
     * @param mixed $article_title
     * @return Reference
     */
    public function setArticleTitle($article_title) {
        $this->article_title = $article_title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJournalTitle() {
        return $this->journal_title;
    }

    /**
     * @param mixed $journal_title
     * @return Reference
     */
    public function setJournalTitle($journal_title) {
        $this->journal_title = $journal_title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVolumeTitle() {
        return $this->volume_title;
    }

    /**
     * @param mixed $volume_title
     * @return Reference
     */
    public function setVolumeTitle($volume_title) {
        $this->volume_title = $volume_title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRefKey() {
        return $this->ref_key;
    }

    /**
     * @param mixed $ref_key
     * @return Reference
     */
    public function setRefKey($ref_key) {
        $this->ref_key = $ref_key;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPublicationYear() {
        return $this->publication_year;
    }

    /**
     * @param mixed $publication_year
     * @return Reference
     */
    public function setPublicationYear($publication_year) {
        $this->publication_year = $publication_year;
        return $this;
    }
    /**
     * @return mixed
     */
    public function getArticle() {
        return $this->article;
    }

    /**
     * @param mixed $article
     * @return Reference
     */
    public function setArticle($article) {
        $this->article = $article;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUnstructured() {
        return $this->unstructured;
    }

    /**
     * @param mixed $unstructured
     * @return Reference
     */
    public function setUnstructured($unstructured) {
        $this->unstructured = trim(preg_replace('!\s+!', ' ', $unstructured));
        return $this;
    }



}