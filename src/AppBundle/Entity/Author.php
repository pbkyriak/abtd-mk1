<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

class Author {

    use TimestampableTrait;

    private $id;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=2)
     */
    private $given;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=2)
     */
    private $family;
    private $affiliation;
    private $article_count=0;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $articles;

    public function __construct() {
        $this->articles = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Author
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGiven() {
        return $this->given;
    }

    /**
     * @param mixed $given
     * @return Author
     */
    public function setGiven($given) {
        $this->given = $given;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFamily() {
        return $this->family;
    }

    /**
     * @param mixed $family
     * @return Author
     */
    public function setFamily($family) {
        $this->family = $family;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAffiliation() {
        return $this->affiliation;
    }

    /**
     * @param mixed $affiliation
     * @return ArticleAuthor
     */
    public function setAffiliation($affiliation) {
        $this->affiliation = $affiliation;
        return $this;
    }

    public function addArticle(Article $article) {
        $this->articles[] = $article;
    }

    public function getArticles() {
        return $this->articles;
    }

    public function removeArticle(Article $article) {
        $this->articles->removeElement($article);
    }

    public function getArticleCount() {
        return $this->article_count;
    }

    public function setArticleCount($count) {
        $this->article_count = $count;
        return $this;
    }
}