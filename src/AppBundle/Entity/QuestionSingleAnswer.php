<?php

namespace AppBundle\Entity;


class QuestionSingleAnswer {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \AppBundle\Entity\QuestionSingleAnswer
     */
    private $question;

    private $article_count=0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return QuestionSingleAnswer
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set question
     *
     * @param \AppBundle\Entity\QuestionSingle $question
     *
     * @return QuestionAnswer
     */
    public function setQuestion(\AppBundle\Entity\QuestionSingle $question = null)
    {
        $this->question = $question;
        return $this;
    }

    /**
     * Get question
     *
     * @return \AppBundle\Entity\QuestionSingle
     */
    public function getQuestion()
    {
        return $this->question;
    }

    public function getArticleCount() {
        return $this->article_count;
    }

    public function setArticleCount($v) {
        $this->article_count = $v;
        return $this;
    }

    public function __toString() {
        return $this->getTitle();
    }
}