<?php

namespace AppBundle\Entity;


class ArticleAuthor {
    private $id;
    private $aorder;
    private $article;
    private $author;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return ArticleAuthor
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAorder() {
        return $this->aorder;
    }

    /**
     * @param mixed $aorder
     * @return ArticleAuthor
     */
    public function setAorder($aorder) {
        $this->aorder = $aorder;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArticle() {
        return $this->article;
    }

    /**
     * @param mixed $article
     * @return ArticleAuthor
     */
    public function setArticle($article) {
        $this->article = $article;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * @param mixed $author
     * @return ArticleAuthor
     */
    public function setAuthor($author) {
        $this->author = $author;
        return $this;
    }
}