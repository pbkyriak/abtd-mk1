<?php

namespace AppBundle\Entity;

use AppBundle\Entity\ReviewQuestion;
use AppBundle\Entity\Traits\ActivityLoggableTrait;
use AppBundle\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Article entity.
 */
class Article
{

    const STATUS_PUBLISHED = 'P';
    const STATUS_DRAFT = 'D';

    use TimestampableTrait;
    //use ActivityLoggableTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @Assert\Count(min=1, minMessage="There must be at least one author")
     * @Assert\Valid()
     */
    private $authors;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @Assert\Valid()
     */
    private $keywords;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=20)
     */
    private $article_type;
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min=4, max=6)
     */
    private $publish_date;
    /**
     * @var string
     */
    private $doi='';
    /**
     * @var string
     * @Assert\Url()
     */
    private $url='';
    /**
     * @var \AppBundle\Entity\Publisher
     * @Assert\Valid()
     */
    private $publisher;
    /**
     * @var \AppBundle\Entity\Journal
     * @Assert\Valid()
     */
    private $journal;
    /**
     * @var string
     */
    private $bibtex;
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     */
    private $references;

    /**
     * @var ArticleReview
     */
    private $review;

    private $owner;

    private $status;

    private $comment_count=0;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
        $this->keywords = new ArrayCollection();
        $this->references = new ArrayCollection();
    }

    /**
     * Get id
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     * @param string $title
     * @return Article
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    public function addAuthor(ArticleAuthor $author) {
        $this->authors[] = $author;
    }

    public function removeAuthor(ArticleAuthor $author) {
        $this->authors->removeElement($author);
    }

    public function getAuthors() {
        return $this->authors;
    }

    /**
     * @return string
     */
    public function getArticleType() {
        return $this->article_type;
    }

    /**
     * @param mixed $article_type
     * @return Article
     */
    public function setArticleType($article_type) {
        $this->article_type = $article_type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPublishDate() {
        return $this->publish_date;
    }

    /**
     * @param mixed $publish_date
     * @return Article
     */
    public function setPublishDate($publish_date) {
        $this->publish_date = $publish_date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDoi() {
        return $this->doi;
    }

    /**
     * @param mixed $doi
     * @return Article
     */
    public function setDoi($doi) {
        $this->doi = $doi;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return Article
     */
    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }

    /**
     * @return Publisher
     */
    public function getPublisher() {
        return $this->publisher;
    }

    /**
     * @param Journal|null $publisher
     * @return Article
     */
    public function setPublisher($publisher) {
        $this->publisher = $publisher;
        return $this;
    }

    public function addKeyword(Keyword $keyword) {
        $this->keywords[] = $keyword;
    }

    public function removeKeyword(Keyword $keyword) {
        $this->keywords->removeElement($keyword);
    }

    public function getKeywords() {
        return $this->keywords;
    }
    /**
     * @return Journal
     */
    public function getJournal() {
        return $this->journal;
    }

    /**
     * @param Journal|null $journal
     */
    public function setJournal($journal) {
        $this->journal = $journal;
    }

    /**
     * @return mixed
     */
    public function getBibtex() {
        return $this->bibtex;
    }

    /**
     * @param string $bibtex
     * @return Article
     */
    public function setBibtex($bibtex) {
        $this->bibtex = $bibtex;
        return $this;
    }

    public function addReference(Reference $reference) {
        $reference->setArticle($this);
        $this->references[] = $reference;
    }

    public function removeReference(Reference $reference) {
        $this->references->removeElement($reference);
    }

    public function setReview($review) {
        $this->review = $review;
        return $this;
    }

    /**
     * @return ArticleReview
     */
    public function getReview() {
        return $this->review;
    }

    public function setOwner($owner) {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return User
     */
    public function getOwner() {
        return $this->owner;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function __toString() {
        return $this->title;
    }

    public function canBePublished() {
        $out = false;
        if(is_object($this->getReview())) {
            $out = true;
        }
        return $out;
    }

    public function getCommentCount() {
        return $this->comment_count;
    }

    public function setCommentCount($count) {
        $this->comment_count = $count;
        return $this;
    }
}
