<?php
namespace AppBundle\Entity;

class QuestionMultiple extends Question {

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $answers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add answer
     *
     * @param \AppBundle\Entity\QuestionAnswer $answer
     *
     * @return QuestionMultiple
     */
    public function addAnswer(\AppBundle\Entity\QuestionAnswer $answer)
    {
        printf("%s<br />",__METHOD__);
        $answer->setQuestion($this);
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \AppBundle\Entity\QuestionAnswer $answer
     */
    public function removeAnswer(\AppBundle\Entity\QuestionAnswer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

}
