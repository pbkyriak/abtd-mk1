<?php

namespace AppBundle\Entity\Repository;


use Doctrine\ORM\Query;

class ActivityLogRepository extends \Doctrine\ORM\EntityRepository {

    public function getLatest($limit=5) {
        $qb = $this->createQueryBuilder('c')
                    ->select("c, u")
                   ->leftJoin('c.user', 'u')
                   ->leftJoin('AppBundle:Article', 'a','WITH', 'c.article_id=a.id')
                   ->orderBy('c.created_at', 'DESC')
                   ->setMaxResults($limit);
        //$results = $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
        $results = $qb->getQuery()->execute();
        return $results;
    }
}