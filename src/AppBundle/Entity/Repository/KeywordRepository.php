<?php

namespace AppBundle\Entity\Repository;

class KeywordRepository extends \Doctrine\ORM\EntityRepository {

    /**
     * Get keyword entity if exists else creates new
     * @param $title
     * @return \AppBundle\Entity\Keyword
     */
    public function getKeywordByTitle($title) {
        $k = $this->findOneBy(['title' => trim(strtolower($title))]);
        return $k;
    }
}