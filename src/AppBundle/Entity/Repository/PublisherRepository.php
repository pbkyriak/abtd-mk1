<?php

namespace AppBundle\Entity\Repository;


class PublisherRepository extends \Doctrine\ORM\EntityRepository {
    public function getByTitle($title) {
        $k = $this->findOneBy(['title' => trim(strtolower($title))]);
        return $k;
    }

}