<?php

namespace AppBundle\Entity\Repository;


class ArticleAuthorRepository extends \Doctrine\ORM\EntityRepository {

    public function getAuthorsArticleCount($authorIds) {
        $sql = "SELECT aa.author_id, COUNT(aa.article_id) AS cnt
                FROM article_authors  aa
                LEFT JOIN article a ON aa.article_id=a.id
                WHERE aa.author_id IN (%s) AND a.status='P'
                GROUP BY aa.author_id";
        $sql = sprintf($sql, implode(',',$authorIds));
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql);
        $results = $stmt->fetchAll();
        $out = array();
        if($results) {
            foreach ($results as $result)
            $out[$result['author_id']] = $result['cnt'];
        }
        return $out;
    }
}