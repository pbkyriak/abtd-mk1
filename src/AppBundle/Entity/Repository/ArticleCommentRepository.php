<?php

namespace AppBundle\Entity\Repository;


class ArticleCommentRepository extends \Doctrine\ORM\EntityRepository {

    public function getLatest($limit=5) {
        $qb = $this->createQueryBuilder('c')
                ->leftJoin('c.article', 'a')
                   ->where('a.status=:st')
                   ->orderBy('c.created_at', 'DESC')
                   ->setMaxResults($limit);
        $qb->setParameter('st', 'P');
        $results = $qb->getQuery()->execute();
        return $results;
    }

}