<?php

namespace AppBundle\Entity\Repository;


class QuestionRepository extends \Doctrine\ORM\EntityRepository {

    public function getFilterQuestions() {
        $results = $this->createQueryBuilder('q')
            ->select('q')
            ->where("(q INSTANCE OF AppBundle:QuestionMultiple or q instance of AppBundle:QuestionSingle)")
            ->andWhere('q.active=1')
            ->andWhere('q.show_filter=1')
            ->orderBy("q.aorder", 'ASC')
            ->getQuery()
            ->execute();
        return $results;
    }
    //$em->getClassMetadata('My\Entity\Class')

    public function getActiveQuestions() {
        $results = $this->createQueryBuilder('q')
            ->select('q')
            ->where('q.active=1')
            ->orderBy("q.aorder", 'ASC')
            ->getQuery()
            ->execute();
        return $results;
    }

    public function getQuestionsSimpleList() {
        return
        $this->createQueryBuilder('q')
            ->select("q.id, q.question")
            ->orderBy('q.aorder')
            ->getQuery()
            ->getArrayResult();
    }


    public function updateArticleCountSingle() {
        $sql1 = "UPDATE question_single_answer qqa SET qqa.article_count=0";
        $this->getEntityManager()->getConnection()->exec($sql1);
        $sql2 = "
                UPDATE question_single_answer qqa SET qqa.article_count=(
                SELECT COUNT(rq.answer_s_id) cnt
                FROM question q
                LEFT JOIN review_question rq ON rq.question_id=q.id AND q.questionType='S'
                LEFT JOIN article_review ar ON ar.id=rq.review_id
                LEFT JOIN article a ON ar.article_id=a.id and a.status='P'
                WHERE not isnull(a.id) and rq.answer_s_id=qqa.id)
                ";
        $this->getEntityManager()->getConnection()->exec($sql2);
    }

    public function updateArticleCountMulti() {
        $sql1 = "UPDATE question_answer qqa SET qqa.article_count=0";
        $this->getEntityManager()->getConnection()->exec($sql1);
        $sql2 = "UPDATE question_answer qqa SET qqa.article_count=(
                  SELECT COUNT(rqa.question_answer_id) cnt 
                    FROM question q
                    LEFT JOIN review_question rq ON rq.question_id=q.id AND q.questionType='M'
                    LEFT JOIN article_review ar ON ar.id=rq.review_id
                    LEFT JOIN article a ON ar.article_id=a.id and a.status='P'
                    LEFT JOIN review_question_answers rqa ON rqa.review_question_id=rq.id
                    WHERE not isnull(a.id) and rqa.question_answer_id=qqa.id)
                ";
        $this->getEntityManager()->getConnection()->exec($sql2);
    }

    public function getActiveMSQuestions() {
        $results = $this->createQueryBuilder('q')
                        ->select('q')
                        ->where("(q INSTANCE OF AppBundle:QuestionMultiple or q instance of AppBundle:QuestionSingle)")
                        ->andWhere('q.active=1')
                        ->orderBy("q.aorder", 'ASC')
                        ->getQuery()
                        ->execute();
        return $results;
    }

}

