<?php

namespace AppBundle\Entity\Repository;


class ArticleRepository extends \Doctrine\ORM\EntityRepository {

    public function getLatest($limit=5) {
        $qb = $this->createQueryBuilder('a')
            ->where('a.status=:st')
            ->orderBy('a.created_at', 'DESC')
            ->setMaxResults($limit);
        $qb->setParameter('st', 'P');
        $results = $qb->getQuery()->execute();
        return $results;
    }

    public function updateArticleCommentCount($articleId) {
        $conn = $this->getEntityManager()->getConnection();
        $count = $conn->fetchColumn("select count(*) from article_comment where article_id=:ai", ['ai'=>$articleId]);
        $conn->update('article',['comment_count'=>$count], ['id'=>$articleId]);
    }
}