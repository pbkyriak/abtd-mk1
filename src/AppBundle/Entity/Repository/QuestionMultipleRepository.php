<?php

namespace AppBundle\Entity\Repository;


use AppBundle\Entity\QuestionMultiple;

class QuestionMultipleRepository extends \Doctrine\ORM\EntityRepository {

    public function getActiveQuestions() {
        return $this->findBy(['active'=>true]);
    }

}