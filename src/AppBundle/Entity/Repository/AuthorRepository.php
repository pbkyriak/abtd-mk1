<?php

namespace AppBundle\Entity\Repository;


class AuthorRepository extends \Doctrine\ORM\EntityRepository {

    public function getByFullName($family, $given, $affiliation) {
        $k = $this->findOneBy(
            [
            'family'=>trim(strtolower($family)),
            'given'=>trim(strtolower($given)),
            'affiliation'=>trim(strtolower($affiliation)),
            ]
        );
        return $k;
    }

    public function updateArticleCount() {
        $sql = "UPDATE author b SET article_count = (SELECT COUNT(a.id) FROM article_authors aa LEFT JOIN article a ON aa.article_id=a.id AND a.status='P' WHERE aa.author_id=b.id)";
        $this->getEntityManager()->getConnection()->exec($sql);
    }
}