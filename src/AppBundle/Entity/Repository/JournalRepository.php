<?php

namespace AppBundle\Entity\Repository;


class JournalRepository extends \Doctrine\ORM\EntityRepository {

    public function getByTitle($title) {
        $k = $this->findOneBy(['title' => trim(strtolower($title))]);
        return $k;
    }
}