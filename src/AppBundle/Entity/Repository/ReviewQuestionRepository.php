<?php

namespace AppBundle\Entity\Repository;


class ReviewQuestionRepository extends \Doctrine\ORM\EntityRepository {

    public function getQuestionMultipleAnswerCount($question) {
        $dql = "SELECT rqa.id qid, count(rqa) qcnt FROM AppBundle:ReviewQuestion rq LEFT JOIN rq.answer rqa WHERE rq.question=:q group by rqa.id";
        $query = $this->getEntityManager()->createQuery($dql)->setParameter('q', $question);
        return $query->getArrayResult();
    }
}