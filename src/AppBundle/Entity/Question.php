<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimestampableTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Question
 */
class Question
{
    use TimestampableTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $question;

    private $aorder = 0;
    private $show_filter=false;

    /**
     * @var boolean
     */
    private $active;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $reviewQuestions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reviewQuestions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return Question
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Question
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add articleQuestion
     *
     * @param \AppBundle\Entity\ReviewQuestion $articleQuestion
     *
     * @return Question
     */
    public function addReviewQuestion(\AppBundle\Entity\ReviewQuestion $reviewQuestion)
    {
        $this->reviewQuestions[] = $reviewQuestion;

        return $this;
    }

    /**
     * Remove reviewQuestion
     *
     * @param \AppBundle\Entity\ReviewQuestion $reviewQuestion
     */
    public function removeArticleQuestion(\AppBundle\Entity\ReviewQuestion $reviewQuestion)
    {
        $this->reviewQuestions->removeElement($reviewQuestion);
    }

    /**
     * Get reviewQuestions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReviewQuestions()
    {
        return $this->reviewQuestions;
    }

    /**
     * @return int
     */
    public function getAorder(): int {
        return $this->aorder;
    }

    /**
     * @param int $aorder
     * @return Question
     */
    public function setAorder(int $aorder): Question {
        $this->aorder = $aorder;
        return $this;
    }

    public function getShowFilter() {
        return $this->show_filter;
    }

    public function setShowFilter($showFilter) {
        $this->show_filter = $showFilter;
        return $this;
    }
}


