<?php
namespace AppBundle\Entity;

class QuestionSingle extends Question {

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $answers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add answer
     *
     * @param \AppBundle\Entity\QuestionSingleAnswer $answer
     *
     * @return QuestionSingle
     */
    public function addAnswer(\AppBundle\Entity\QuestionSingleAnswer $answer)
    {
        $answer->setQuestion($this);
        $this->answers[] = $answer;
        return $this;
    }

    /**
     * Remove answer
     *
     * @param \AppBundle\Entity\QuestionSingleAnswer $answer
     */
    public function removeAnswer(\AppBundle\Entity\QuestionSingleAnswer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }
}
