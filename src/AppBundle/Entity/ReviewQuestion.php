<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimestampableTrait;
/**
 * ArticleQuestion
 */
class ReviewQuestion
{
    use TimestampableTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $not_apply = false;

    /**
     * @var float
     */
    private $answer_n;

    /**
     * @var string
     */
    private $answer_t;

    /**
     * @var \AppBundle\Entity\QuestionAnswer
     */
    private $answer_s;

    /**
     * @var \AppBundle\Entity\ArticleReview
     */
    private $review;

    /**
     * @var \AppBundle\Entity\Question
     */
    private $question;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $answer;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answer = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set notApply
     *
     * @param boolean $notApply
     *
     * @return ReviewQuestion
     */
    public function setNotApply($notApply)
    {
        $this->not_apply = $notApply;

        return $this;
    }

    /**
     * Get notApply
     *
     * @return boolean
     */
    public function getNotApply()
    {
        return $this->not_apply;
    }

    /**
     * Set answerN
     *
     * @param float $answerN
     *
     * @return ReviewQuestion
     */
    public function setAnswerN($answerN)
    {
        $this->answer_n = $answerN;

        return $this;
    }

    /**
     * Get answerN
     *
     * @return float
     */
    public function getAnswerN()
    {
        return $this->answer_n;
    }

    /**
     * Set answerT
     *
     * @param string $answerT
     *
     * @return ReviewQuestion
     */
    public function setAnswerT($answerT)
    {
        $this->answer_t = $answerT;

        return $this;
    }

    /**
     * Get answerT
     *
     * @return string
     */
    public function getAnswerT()
    {
        return $this->answer_t;
    }

    public function setAnswerS($answerS) {
        $this->answer_s = $answerS;
        return $this;
    }

    /**
     * @return QuestionAnswer
     */
    public function getAnswerS() {
        return $this->answer_s;
    }
    /**
     * Set article
     *
     * @param \AppBundle\Entity\ArticleReview $review
     *
     * @return ReviewQuestion
     */
    public function setReview(\AppBundle\Entity\ArticleReview $review = null)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get article
     *
     * @return \AppBundle\Entity\ArticleReview
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set question
     *
     * @param \AppBundle\Entity\Question $question
     *
     * @return ReviewQuestion
     */
    public function setQuestion(\AppBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \AppBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Add answer
     *
     * @param \AppBundle\Entity\QuestionAnswer $answer
     *
     * @return ReviewQuestion
     */
    public function addAnswer(\AppBundle\Entity\QuestionAnswer $answer)
    {
        $answer->setQuestion($this);
        $this->answer[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \AppBundle\Entity\QuestionAnswer $answer
     */
    public function removeAnswer(\AppBundle\Entity\QuestionAnswer $answer)
    {
        $answer->setQuestion(null);
        $this->answer->removeElement($answer);
    }

    /**
     * Get answer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    public function getTheAnswer() {
        if($this->getQuestion() instanceof QuestionNumber ) {
            return $this->getAnswerN();
        }
        elseif($this->getQuestion() instanceof QuestionText ) {
            return $this->getAnswerT();
        }
        elseif($this->getQuestion() instanceof QuestionSingle ) {
            $out = '';
            $ans = $this->getAnswer();
            foreach ($ans as $an) {
                $out .= $an->getTitle();
            }
            return $out;
        }
        elseif($this->getQuestion() instanceof QuestionMultiple ) {
            $pts = [];
            $ans = $this->getAnswer();
            foreach ($ans as $an) {
                $pts[] = $an->getTitle();
            }
            return implode(', ', $pts);
        }
        else {
            return '--';
        }
    }
}
