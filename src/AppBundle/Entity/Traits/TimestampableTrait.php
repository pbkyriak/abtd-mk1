<?php

namespace AppBundle\Entity\Traits;


trait TimestampableTrait {

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * Set createdAt
     * @param \DateTime $createdAt
     * @return Article
     */
    public function setCreatedAt($createdAt) {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     * @param \DateTime $updatedAt
     * @return Article
     */
    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    public function setCreatedAtValue() {
        $this->setCreatedAt(new \DateTime());
    }

    public function setUpdatedAtValue() {
        $this->setUpdatedAt(new \DateTime());
    }
}