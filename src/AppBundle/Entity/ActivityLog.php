<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimestampableTrait;

class ActivityLog {

    use TimestampableTrait;

    private $id;
    private $article_id;
    private $action;
    private $user;
    private $title;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return ActivityLog
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArticleId() {
        return $this->article_id;
    }

    /**
     * @param mixed $object_id
     * @return ActivityLog
     */
    public function setArticleId($object_id) {
        $this->article_id = $object_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction() {
        return $this->action;
    }

    /**
     * @param mixed $action
     * @return ActivityLog
     */
    public function setAction($action) {
        $this->action = $action;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param User $user
     * @return ActivityLog
     */
    public function setUser($user) {
        $this->user = $user;
        return $this;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

}