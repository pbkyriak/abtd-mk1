<?php

namespace AppBundle\Entity;

use AppBundle\Entity\ReviewQuestion;
use AppBundle\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

class ArticleReview {

    use TimestampableTrait;
    /**
     * @var integer
     */
    private $id;

    private $review_text;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $questions;

    private $article;

    private $srch_answer_idx;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ArticleReview
     */
    public function setId(int $id): ArticleReview {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReviewText() {
        return $this->review_text;
    }

    /**
     * @param mixed $review
     * @return ArticleReview
     */
    public function setReviewText($reviewText) {
        $this->review_text = $reviewText;
        return $this;
    }

    /**
     * Add question
     * @param ReviewQuestion $question
     * @return Article
     */
    public function addQuestion(ReviewQuestion $question) {
        $question->setReview($this);
        $this->questions[] = $question;
        return $this;
    }

    /**
     * Remove question
     * @param ReviewQuestion $question
     */
    public function removeQuestion(ReviewQuestion $question) {
        $question->setReview(null);
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions() {
        return $this->questions;
    }

    public function setArticle($article) {
        $article->setReview($this);
        $this->article = $article;
        return $this;
    }

    public function getArticle() {
        return $this->article;
    }

    public function getSrchAnswerIdx() {
        return $this->srch_answer_idx;
    }

    public function setSrchAnswerIdx($v) {
        $this->srch_answer_idx=$v;
        return $this;
    }

}