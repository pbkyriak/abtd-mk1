<?php

namespace AppBundle\Entity;

/**
 * ArticleQuestion
 */
class ArticleQuestion
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $not_apply = 0;

    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var float
     */
    private $answer_n;

    /**
     * @var string
     */
    private $answer_t;

    /**
     * @var \AppBundle\Entity\Article
     */
    private $article;

    /**
     * @var \AppBundle\Entity\Question
     */
    private $question;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $answer;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answer = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set notApply
     *
     * @param boolean $notApply
     *
     * @return ArticleQuestion
     */
    public function setNotApply($notApply)
    {
        $this->not_apply = $notApply;

        return $this;
    }

    /**
     * Get notApply
     *
     * @return boolean
     */
    public function getNotApply()
    {
        return $this->not_apply;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ArticleQuestion
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ArticleQuestion
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set answerN
     *
     * @param float $answerN
     *
     * @return ArticleQuestion
     */
    public function setAnswerN($answerN)
    {
        $this->answer_n = $answerN;

        return $this;
    }

    /**
     * Get answerN
     *
     * @return float
     */
    public function getAnswerN()
    {
        return $this->answer_n;
    }

    /**
     * Set answerT
     *
     * @param string $answerT
     *
     * @return ArticleQuestion
     */
    public function setAnswerT($answerT)
    {
        $this->answer_t = $answerT;

        return $this;
    }

    /**
     * Get answerT
     *
     * @return string
     */
    public function getAnswerT()
    {
        return $this->answer_t;
    }

    /**
     * Set article
     *
     * @param \AppBundle\Entity\Article $article
     *
     * @return ArticleQuestion
     */
    public function setArticle(\AppBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \AppBundle\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set question
     *
     * @param \AppBundle\Entity\Question $question
     *
     * @return ArticleQuestion
     */
    public function setQuestion(\AppBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \AppBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Add answer
     *
     * @param \AppBundle\Entity\QuestionAnswer $answer
     *
     * @return ArticleQuestion
     */
    public function addAnswer(\AppBundle\Entity\QuestionAnswer $answer)
    {
        $this->answer[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \AppBundle\Entity\QuestionAnswer $answer
     */
    public function removeAnswer(\AppBundle\Entity\QuestionAnswer $answer)
    {
        $this->answer->removeElement($answer);
    }

    /**
     * Get answer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswer()
    {
        return $this->answer;
    }
}
