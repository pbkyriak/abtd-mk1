<?php
/**
 * @copyright 2017 Kartpay.com.
 * @author Panos <panos@kartpay.com>
 * Date: 16/12/2017
 * Time: 5:13 μμ
 */

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

class Publisher {

    use TimestampableTrait;

    private $id;
    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $articles;

    public function __construct() {
        $this->articles = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Publisher
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Publisher
     */
    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    public function addArticle(Article $article) {
        $this->articles[] = $article;
        return $this;
    }

    public function removeArticle(Article $article) {
        $this->articles->removeElement($article);
        return $this;
    }

}