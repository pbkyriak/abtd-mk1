<?php

namespace AppBundle\Exception;


class NotSupportedQuestionTypeException extends \Exception {

    public function __construct($type) {
        parent::__construct(sprintf("Unsupported Question type: '%s'",$type));
    }
}