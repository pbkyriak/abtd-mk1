<?php
namespace AppBundle\Exception;

class NotSupportedBibtexEntryType extends \Exception {

    public function __construct($entryType) {
        parent::__construct(sprintf("Unsupported bibTex Entry type: '%s'",$entryType));
    }

}