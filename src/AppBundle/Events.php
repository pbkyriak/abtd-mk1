<?php

namespace AppBundle;


final class Events {

    const COMMENT_CREATED = 'comment.created';
    const COMMENT_UPDATED = 'comment.updated';
    const COMMENT_DELETED = 'comment.deleted';
    const ARTICLE_CREATED = 'article.created';
    const ARTICLE_UPDATED = 'article.updated';
    const ARTICLE_DELETED = 'article.deleted';
    const ARTICLE_PUBLISHED = 'article.published';
    const ARTICLE_UNPUBLISHED = 'article.unpublished';
}