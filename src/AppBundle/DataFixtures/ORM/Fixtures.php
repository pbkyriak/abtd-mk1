<?php
/**
 * @copyright 2017 Kartpay.com.
 * @author Panos <panos@kartpay.com>
 * Date: 15/10/2017
 * Time: 4:15 μμ
 */

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Question;
use AppBundle\Entity\QuestionNumber;
use AppBundle\Entity\QuestionText;
use AppBundle\Entity\QuestionSingle;
use AppBundle\Entity\QuestionMultiple;
use AppBundle\Entity\QuestionAnswer;
use AppBundle\Entity\Article;
use AppBundle\Entity\ReviewQuestion;
use AppBundle\Entity\ArticleQuestionAnswer;

class Fixtures extends Fixture {
    public function load(ObjectManager $manager) {
        
        $q1 = new QuestionNumber();
        $q1
            ->setQuestion('How old are you?')
            ->setActive(true)
        ;        
        $manager->persist($q1);
        $manager->flush();

        $q2 = new QuestionText();
        $q2
            ->setQuestion('Whats your name?')
            ->setActive(true)
        ;        
        $manager->persist($q2);
        $manager->flush();
        
        $q3 = new QuestionSingle();
        $q3->setQuestion('Semester of publication')->setActive(true);
        $a1 = new QuestionAnswer();
        $a1->setTitle('First');
        $q3->addAnswer($a1);
        $a2 = new QuestionAnswer();
        $a2->setTitle('Second');
        $q3->addAnswer($a2);
        $a3 = new QuestionAnswer();
        $a3->setTitle('Third');
        $q3->addAnswer($a3);
        $a4 = new QuestionAnswer();
        $a4->setTitle('Fourth');
        $q3->addAnswer($a4);
        $manager->persist($q3);
        $manager->flush();
        
        $q4 = new QuestionMultiple();
        $q4->setQuestion('Foods you like')->setActive(true);
        $a1 = new QuestionAnswer();
        $a1->setTitle('Pasta');
        $q4->addAnswer($a1);
        $a2 = new QuestionAnswer();
        $a2->setTitle('Mousaka');
        $q4->addAnswer($a2);
        $a3 = new QuestionAnswer();
        $a3->setTitle('Fish sticks');
        $q4->addAnswer($a3);
        $a4 = new QuestionAnswer();
        $a4->setTitle('Pork chops');
        $q4->addAnswer($a4);
        $a5 = new QuestionAnswer();
        $a5->setTitle('Bean soup');
        $q4->addAnswer($a5);
        $manager->persist($q4);
        $manager->flush();
        /*
                $a = new Article();
                $a->setTitle('article 1');
                $a->setAbstract('abstract article 1');
                $aq1 = new ArticleQuestion();
                $aq1->setQuestion($q1);
                $aq1->setArticle($a);
                $a->addQuestion($aq1);
                $aq1->addAnswer((new ArticleQuestionAnswer())->setAnswerT('asd'));
                $manager->persist($a);
                $manager->flush();

                $a = new Article();
                $aq1 = new ArticleQuestion();
                $a->addQuestion($aq1);
                $q1 =
                $aq1->setQuestion($q1);
                $aq1->setAnswer(1);

                $aq2 = new ArticleQuestion();
                $a->addQuestion($aq2);
                $q2 =
                $aq2->setQuestion($q2);
                $aq2->setAnswer('text');

                $aq3 = new ArticleQuestion();
                $a->addQuestion($aq3);
                $q3 =
                $aq3->setQuestion($q3);
                $aq3->setAnswer($ans3);

                $aq4 = new ArticleQuestion();
                $a->addQuestion($aq4);
                $q4 =
                $aq4->setQuestion($q4);
                $aq4->addAnswer($ans4a);
                $aq4->addAnswer($ans4b);
                $aq4->addAnswer($ans4c);


                $aq1 = $a->addQuestion($q1);
                $aq1->setAnswer('text');
                */
    }
}