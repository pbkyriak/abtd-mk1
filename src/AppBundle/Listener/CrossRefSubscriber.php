<?php

namespace AppBundle\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use AppBundle\Event\BibtexProcessedEvent;

class CrossRefSubscriber implements EventSubscriberInterface {
    private $em;
    private $tokenStorage;
    private $trans;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->trans = new \AppBundle\Transformer\CrossRefOrgReferencesToEntities();
    }

    public static function getSubscribedEvents() {
        return [
            BibtexProcessedEvent::NAME => 'onArticleParsed',
        ];
    }

    private function getUser() {
        $user = null;

        if($this->tokenStorage->getToken()) {
            $user = $this->tokenStorage->getToken()->getUser();
            if(!is_object($user)) {
                $user = null;
            }
        }
        return $user;

    }

    public function onArticleParsed(BibtexProcessedEvent $event) {
        $doi = $event->getArticle()->getDoi();
        if(!empty($doi)) {
            $data  = $this->trans->do($doi, $this->getUser()->getEmail());
            if($data) {
                foreach($data as $r) {
                    $event->getArticle()->addReference($r);
                }
            }
        }
    }
}