<?php

namespace AppBundle\Listener;

use AppBundle\Entity\ArticleReview;
use AppBundle\Entity\QuestionMultiple;
use AppBundle\Entity\QuestionSingle;
use AppBundle\Entity\ReviewQuestion;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class ArticleReviewAnswerIndexSubscriber implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'preUpdate',
        );
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $this->update($args);
    }

    public function prePersist(LifecycleEventArgs $args) {
        $this->update($args);
    }

    private function update(LifecycleEventArgs $args) {
        $entity = $args->getObject();

        if($entity instanceof ArticleReview) {
            $srchAr = [];
            foreach($entity->getQuestions() as $rquestion) {
                $question = $rquestion->getQuestion();
                if ($question instanceof QuestionMultiple) {
                    foreach($rquestion->getAnswer() as $answer) {
                        $srchAr[] = sprintf("_%s_%s", $question->getId(), $answer->getId());
                    }
                }
                if ($question instanceof QuestionSingle) {
                   $srchAr[] = sprintf("_%s_%s", $question->getId(), $rquestion->getAnswerS()->getId());
                }
            }
            $srch = implode(' ', $srchAr);
            $entity->setSrchAnswerIdx($srch);
        }
    }

}