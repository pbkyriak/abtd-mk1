<?php

namespace AppBundle\Listener;

use AppBundle\Entity\Article;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;
use AppBundle\Events;
use Doctrine\ORM\EntityManagerInterface;

class AuthorArticleCountSubscriber implements EventSubscriberInterface {

    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public static function getSubscribedEvents() {
        return [
            Events::ARTICLE_PUBLISHED => 'onArticleChanged',
            Events::ARTICLE_UNPUBLISHED => 'onArticleChanged',
            Events::ARTICLE_DELETED => 'onArticleDeleted',
        ];
    }

    public function onArticleChanged(GenericEvent $event) {
        $subject = $event->getSubject();
        if($subject instanceof Article) {
            $this->em->getRepository("AppBundle:Author")->updateArticleCount();
        }
    }

    public function onArticleDeleted(GenericEvent $event) {
        $subject = $event->getSubject();
        if(!is_object($subject)) {
            $this->em->getRepository("AppBundle:Author")->updateArticleCount();
        }
    }


}