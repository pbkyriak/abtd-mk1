<?php

namespace AppBundle\Listener;

use AppBundle\Entity\Article;
use AppBundle\Entity\ArticleComment;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;
use AppBundle\Events;

class ArticleCommentCountSubscriber implements EventSubscriberInterface {

    private $repository;

    public function __construct($repository) {
        $this->repository = $repository;
    }

    public static function getSubscribedEvents() {
            return [
                Events::COMMENT_CREATED => 'onCommentCreated',
                Events::COMMENT_DELETED => 'onCommentDeleted',
            ];
    }

    public function onCommentCreated(GenericEvent $event) {
        $subject = $event->getSubject();
        if($subject instanceof ArticleComment) {
            $article = $subject->getArticle();
            if($article instanceof Article) {
                $articleId = $article->getId();
                $this->repository->updateArticleCommentCount($articleId);
            }
        }
    }

    public function onCommentDeleted(GenericEvent $event) {
        $subject = $event->getSubject();
        if($subject instanceof Article) {
            $articleId = $subject->getId();
            $this->repository->updateArticleCommentCount($articleId);
        }
    }

}