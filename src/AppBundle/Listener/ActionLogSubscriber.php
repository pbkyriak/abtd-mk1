<?php

namespace AppBundle\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

use AppBundle\Events;
use AppBundle\Entity\ActivityLog;

class ActionLogSubscriber implements EventSubscriberInterface {
    private $em;
    private $tokenStorage;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    public static function getSubscribedEvents() {
        return [
            Events::ARTICLE_PUBLISHED => 'onArticlePublished',
            Events::COMMENT_CREATED => 'onCommentCreated',
        ];
    }

    public function onArticlePublished(GenericEvent $event) {
        $this->saveAction(
            $event->getSubject()->getId(),
            $event->getSubject()->getTitle(),
            'Published'
        );
    }

    public function onCommentCreated(GenericEvent $event) {
        $this->saveAction(
            $event->getSubject()->getArticle()->getId(),
            $event->getSubject()->getArticle()->getTitle(),
            'Commented'
        );
    }

    private function saveAction($id, $title, $action) {
        $logObj = new ActivityLog();
        $logObj->setUser($this->getUser())
               ->setArticleId($id)
               ->setTitle($title)
               ->setAction($action)
               ->setCreatedAt(new \DateTime())
        ;
        $this->em->persist($logObj);
        $this->em->flush($logObj);
    }
    private function getUser() {
        $user = null;

        if($this->tokenStorage->getToken()) {
            $user = $this->tokenStorage->getToken()->getUser();
            if(!is_object($user)) {
                $user = null;
            }
        }
        return $user;

    }
}