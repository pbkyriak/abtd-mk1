<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ArticleQuestionAnswer;
use AppBundle\Entity\ArticleReview;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\ArticleReviewType;
use AppBundle\Entity\Article;
use AppBundle\Entity\ReviewQuestion;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\ConstraintViolation;

use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;

/**
 * Class DefaultController
 * @Breadcrumb("Home", route="home_page")
 */
class DefaultController extends Controller
{

    /**
     * Homepage action
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine();
        $lastestActicles = $em->getRepository('AppBundle:Article')->getLatest(4);
        $statPerType = [
            'Journal' => 12,
            'Conference' => 49,
            'Book' => 29,
        ];
        $statPerType = $this->getShareByType();
        return $this->render('AppBundle:Default:index.html.twig', [
            'articles' => $lastestActicles,
            'stat_per_type' => $statPerType,
        ]);
    }

    public function latestActivitiesAction(Request $request) {
        $result = $this->getDoctrine()->getRepository("AppBundle:ActivityLog")->getLatest(5);
        return $this->render(
            'AppBundle:Default:latest_actions.html.twig',
            ['actions' => $result,]
        );

    }

    private function getShareByType() {
        $trans = $this->container->get('transformer.bibtex_to_article');
        $articleTypes = $trans->getTransformerKeys();
        $stmt = $this->getDoctrine()->getConnection()->executeQuery("SELECT article_type, COUNT(id) cnt FROM article WHERE STATUS='P' GROUP BY article_type");
        $results = $stmt->fetchAll(\PDO::FETCH_KEY_PAIR);
        $out = [];
        $total = array_sum($results);
        foreach ($articleTypes as $type) {
            $out[$type]= isset($results[$type]) ? $results[$type] : 0;
            $out[$type] = round(100*$out[$type]/$total,0);
        }
        return $out;
    }
}
