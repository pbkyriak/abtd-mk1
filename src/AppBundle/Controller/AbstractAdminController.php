<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AbstractAdminController extends Controller {

    protected $isAdmin;
    protected $loggedUser;
    protected $shop;

    protected function isLoggedUserAdmin() {
        return $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN');
    }

    protected function getLoggedUserEntity() {
        /*
        $em = $this->getDoctrine()->getManager();
        $secUser = $this->getUser();
        $user = $em->getRepository('AppBundle:User')->find($secUser->getId());
        */
        return $this->getUser();
    }


    protected function actionPre(Request $request) {
        $out = false;
        $this->isAdmin = $this->isLoggedUserAdmin();
        $this->loggedUser = $this->getLoggedUserEntity();
        return $out;
    }

}