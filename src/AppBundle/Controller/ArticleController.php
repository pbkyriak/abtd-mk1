<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ArticleComment;
use AppBundle\Entity\ArticleQuestionAnswer;
use AppBundle\Entity\ArticleReview;
use AppBundle\Entity\QuestionMultiple;
use AppBundle\Events;
use AppBundle\Form\ArticleCommentFormType;
use AppBundle\Form\FltArticleReviewType;
use AppBundle\Helper\FltEditor;
use AppBundle\Helper\FltMeta;
use AppBundle\Helper\FltQuestions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\GenericEvent;

use AppBundle\Form\ArticleReviewType;
use AppBundle\Entity\Article;
use AppBundle\Entity\ReviewQuestion;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Class ArticleController
 * @Breadcrumb("Home", route="home_page")
 * @Breadcrumb("Articles", route="articles")
 * @CurrentMenuItem("articles")
 */
class ArticleController extends AbstractAdminController {

    public function indexAction(Request $request) {
        $this->actionPre($request);

        $filters = $this->get("AppBundle\ArticleFilters\ArticleFilters");

        $form = $this->createForm(FltArticleReviewType::class, $filters->getFilter('questions')->getQuestions());

        $form->submit($request->get($form->getName()));

        $data = $form->getData();
        $qb = $filters->applyFilters($data, $request->query->all());

        $query = $qb->getQuery();
        //echo $query->getSQL();die(1);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1) /* page number */,
            $request->query->get('limit', 3)
        );

        return $this->render(
            'AppBundle:Article:index.html.twig',
            [
                'pagination'=>$pagination,
                'filterForm' => $form->createView(),
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Breadcrumb("Create")
     */
    public function createArticleAction(Request $request) {
        $defaultData = array('bibtex' => '');
        $form = $this->createFormBuilder($defaultData)
                     ->add('bibtex', TextareaType::class, ['label'=>'bibtex'])
                     ->add('save', SubmitType::class, array('label' => 'slx_metronic.general.save', 'attr' => array('class' => 'btn blue')))
                     ->getForm();

        if( $request->isMethod('POST')) {
            $form->handleRequest($request);
            if($form->isValid()) {
                $data = $form->getData();
                try {
                    $trans = $this->container->get('transformer.bibtex_to_article');
                    $article = $trans->toArticle($data['bibtex']);
                    //$article->setAbstract('dd');
                    $errors = $this->container->get('validator')->validate($article);
                    if(count($errors)) {
                        /** @var Symfony\Component\Validator\ConstraintViolation $error */
                        foreach($errors as $error) {
                            $this->addFlash('error', sprintf('@%s: %s',$error->getPropertyPath(), $error->getMessage()));
                        }
                    }
                    else {
                        $article->setOwner($this->getUser());
                        $article->setStatus(Article::STATUS_DRAFT);
                        $this->getDoctrine()->getManager()->persist($article);
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('info', 'record saved');
                        return $this->redirect($this->generateUrl('review_create', ['article_id' => $article->getId()]));
                    }
                }
                catch(\Exception $ex) {
                    $this->addFlash('error', $ex->getMessage());
                }
            }
            else {
                $this->addFlash('error', 'errors in the form');
            }
        }

        return $this->render('AppBundle:Article:create_article.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    public function createReviewAction(Request $request, $article_id, EventDispatcherInterface $eventDispatcher) {
        $em = $this->getDoctrine();
        $questions = $em->getRepository('AppBundle:Question')->getActiveQuestions();

        $article = $em->getRepository("AppBundle:Article")->find($article_id);
        if(is_object($article->getReview())) {
            //return $this->redirect($this->generateUrl('article_show', ['article_id'=>$article->getId()]));
            $review = $article->getReview();
            $isNewReview = false;
        }
        else {
            $isNewReview=true;
            $review = new ArticleReview();
            $review->setArticle($article);

            foreach ($questions as $question) {
                $aq = new ReviewQuestion();
                $aq->setReview($review);
                $aq->setQuestion($question);
                $review->addQuestion($aq);
            }
        }

        $form = $this->createForm(ArticleReviewType::class, $review);
        if( $request->isMethod('POST')) {
            $form->handleRequest($request);

            if($form->isValid()) {
                $article->setStatus(Article::STATUS_DRAFT);
                if($isNewReview) {
                    $review->setArticle($article);
                    $article->setReview($review);
                }
                $this->getDoctrine()->getManager()->persist($article);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('info', 'record saved');
                $event = new GenericEvent($article);
                $eventDispatcher->dispatch($isNewReview ? Events::ARTICLE_CREATED : Events::ARTICLE_UPDATED, $event);
                return $this->redirect($this->generateUrl('article_show', ['article_id'=>$article->getId()]));
            }
            else {
                $this->addFlash('error', 'errors in the form');
            }
        }
        //return $this->render('SlxShopBundle:Shop:update.html.twig', ['form'=>$form->createView(), 'entity'=>$entity]);

        return $this->render('AppBundle:Article:create_review.html.twig', [
            'form' => $form->createView(),
            'article' => $article,
            'isNewReview' => $isNewReview,
        ]);
    }

    /**
     * @param Request $request
     * @param         $article_id
     * @return \Symfony\Component\HttpFoundation\Response

     * @CurrentMenuItem("articles")
     */
    public function showAction(Request $request, $article_id) {
        $em = $this->getDoctrine();
        $article = $em->getRepository("AppBundle:Article")->find($article_id);
        $comments = $em->getRepository("AppBundle:ArticleComment")->findBy(['article'=>$article], ['created_at'=>'DESC']);
        $this->get("apy_breadcrumb_trail")->add($article->getTitle());
        return $this->render('AppBundle:Article:show.html.twig', [
            'article' => $article,
            'comments' => $comments,
        ]);
    }

    public function deleteAction(Request $request, $article_id, EventDispatcherInterface $eventDispatcher) {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository("AppBundle:Article")->find($article_id);
        if($article) {
            $em->remove($article);
            $em->flush();
            $event = new GenericEvent($article_id);
            $eventDispatcher->dispatch(Events::ARTICLE_DELETED, $event);
        }
        return $this->redirect($this->generateUrl('articles'));
    }

    public function switchStatusAction(Request $request, $article_id, $status, EventDispatcherInterface $eventDispatcher) {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository("AppBundle:Article")->find($article_id);
        if($article) {
            if( ($status==Article::STATUS_PUBLISHED && $article->canBePublished()) || $status==Article::STATUS_DRAFT) {
                $article->setStatus($status);
                $em->persist($article);
                $em->flush();
                $msg = $status==Article::STATUS_PUBLISHED ? 'app.article.article_published' : 'app.article.article_unpublished';
                $this->addFlash('info', $msg);
                $event = new GenericEvent($article);
                $eventDispatcher->dispatch(
                    $status==Article::STATUS_PUBLISHED ? Events::ARTICLE_PUBLISHED : Events::ARTICLE_UNPUBLISHED,
                    $event
                );
            }
        }
        return $this->redirect($this->generateUrl('article_show', ['article_id'=>$article->getId()]));
    }

}