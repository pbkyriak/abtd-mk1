<?php

namespace AppBundle\Controller;
use AppBundle\Entity\QuestionAnswer;
use AppBundle\Entity\QuestionMultiple;
use AppBundle\Entity\QuestionNumber;
use AppBundle\Entity\QuestionSingle;
use AppBundle\Entity\QuestionSingleAnswer;
use AppBundle\Entity\QuestionText;
use AppBundle\Helper\QuestionTypes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;

/**
 * Description of ShopController
 * @Breadcrumb("Home", route="slx_metronic_homepage")
 * @Breadcrumb("Articles", route="slx_metronic_homepage")
 * @Breadcrumb("app.questions.questions", route="questions")
 * @CurrentMenuItem("questions")
 */
class QuestionController extends AbstractAdminController {

    public function indexAction(Request $request) {
        $this->actionPre($request);
        $query = $this->makeListQuery();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1) /* page number */,
            $request->query->get('limit', 15)
        );

        $reorderQs = $this->getDoctrine()->getRepository('AppBundle:Question')->getQuestionsSimpleList();
        return $this->render(
            'AppBundle:Question:index.html.twig',
            [
                'pagination'=>$pagination,
                'reorderQs' => $reorderQs,
            ]
        );

    }

    /**
     * Create list query
     */
    private function makeListQuery() {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM AppBundle:Question a ORDER BY a.aorder";
        $query = $em->createQuery($dql);
        return $query;
    }

    public function newAction(Request $request, $type) {
        $this->actionPre($request);
        $this->get("apy_breadcrumb_trail")->add("slx_metronic.general.add");

        $entityClass = $this->container->get('app.question_types_manager')->discrToEntityType($type);
        $formType = $this->container->get('app.question_types_manager')->discrToFormType($type);
        $entity = new $entityClass();
        if($entity instanceof QuestionSingle) {
            $entity->addAnswer(new QuestionSingleAnswer());
        }
        elseif($entity instanceof QuestionMultiple) {
            $entity->addAnswer(new QuestionAnswer());
        }
        $form = $this->createForm($formType, $entity);
        if( $request->isMethod('POST')) {
            $form->handleRequest($request);
            if($form->isValid()) {
                $this->getDoctrine()->getManager()->persist($entity);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('info', 'record saved');
                if( $form->get('saveAndAdd')->isClicked() ) {
                    return $this->redirect($this->generateUrl('questions_new', ['type'=>$type]));
                }
                return $this->redirect($this->generateUrl('questions_update', ['id'=>$entity->getId()]));
            }
        }
        return $this->render('AppBundle:Question:update.html.twig', ['form'=>$form->createView(), 'entity'=>$entity, 'type'=>$type]);
    }

    /**
     * @param Request $request
     * @param type $id
     * @return type
     * @throws type
     */
    public function updateAction(Request $request, $id) {
        $this->actionPre($request);
        $this->get("apy_breadcrumb_trail")->add("slx_metronic.general.edit");
        $em = $this->getDoctrine()->getManager();
        $isAdmin = $this->isLoggedUserAdmin();
        $entity = $em->getRepository('AppBundle:Question')->find($id); //, $this->getLoggedUserEntity(), $isAdmin);
        if( !$entity ) {
            throw $this->createNotFoundException('Unable to find Question entity.');
        }
        $qtm = $this->container->get('app.question_types_manager');
        $formType = $qtm->discrToFormType($entity);
        $form = $this->createForm($formType, $entity);
        if( $request->isMethod('POST')) {
            $form->handleRequest($request);
            if($form->isValid()) {
                $this->getDoctrine()->getManager()->persist($entity);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('info', 'record saved');
                if( $form->get('saveAndAdd')->isClicked() ) {
                    return $this->redirect($this->generateUrl('questions_new'));
                }
                return $this->redirect($this->generateUrl('questions_update', ['id'=>$entity->getId()]));
            }
        }
        return $this->render('AppBundle:Question:update.html.twig', ['form'=>$form->createView(), 'entity'=>$entity]);

    }

    public function deleteAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $isAdmin = $this->isLoggedUserAdmin();
        $entity = $em->getRepository('AppBundle:Question')->find($id); //, $this->getLoggedUserEntity(), $isAdmin);
        if( !$entity ) {
            throw $this->createNotFoundException('Unable to find Question entity.');
        }
        $em->remove($entity);
        $em->flush();
        $this->addFlash('info', 'app.questions.deleted');
        return $this->redirect($this->generateUrl('questions'));
    }

    public function reorderAction(Request $request) {
        $itmString = str_replace("item_", "", $request->get('item_order_str'));
        $ids = explode(',', $itmString);
        $ids = array_map('intval',$ids);
        $aorder=10;
        $conn = $this->getDoctrine()->getConnection();
        foreach($ids as $id) {
            $conn->update('question', ['aorder'=>$aorder],['id'=>$id]);
            $aorder +=10;
        }
        $this->addFlash('info', 'app.questions.deleted');
        return $this->redirect($this->generateUrl('questions'));
    }
}
