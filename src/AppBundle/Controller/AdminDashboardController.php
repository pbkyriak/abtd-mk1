<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ArticleQuestionAnswer;
use AppBundle\Entity\ArticleReview;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\ArticleReviewType;
use AppBundle\Entity\Article;
use AppBundle\Entity\ReviewQuestion;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\ConstraintViolation;

use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;

/**
 * Class DefaultController
 * @Breadcrumb("Home", route="slx_metronic_homepage")
 */
class AdminDashboardController extends Controller
{

    public function indexAction(Request $request)
    {
        return $this->render('AppBundle:AdminDashboard:index.html.twig', [
        ]);
    }

}