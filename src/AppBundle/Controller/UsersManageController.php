<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserFormType;
use Doctrine\ORM\EntityNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class UsersManageController
 * @Breadcrumb("Home", route="slx_metronic_homepage")
 * @Breadcrumb("slx_metronic.menu.settings", route="users_manage")
 * @Breadcrumb("users.user.users", route="users_manage")
 * @CurrentMenuItem("users_manage")
 */
class UsersManageController extends AbstractAdminController {

    /**
     * @param Request $request
     * @return array
     * @Template()
     */
    public function indexAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:User')->getUsersQuery($request);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1) /* page number */,
            $request->query->get('limit', 15)
        );

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * Finds and displays a User entity.
     *
     * @Template()
     * @Breadcrumb("Προβολή")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        return array(
            'entity' => $entity,
        );
    }

    /**
     * @param         $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws EntityNotFoundException
     * @Template("AppBundle:UsersManage:edit.html.twig")
     */
    public function editAction($id, Request $request) {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(['id'=>$id]);
        if (!is_object($user)) {
            throw new EntityNotFoundException('This user does not exist.');
        }

        $form = $this->createForm(UserFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var $userManager UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);
            $url = $this->generateUrl('users_manage_show', ['id'=> $user->getId()]);
            $response = new RedirectResponse($url);

            return $response;
        }

        return array(
            'entity' => $user,
            'form' => $form->createView(),
        );
    }

    /**
     * @param Request $request
     * @return array|RedirectResponse
     * @Template("AppBundle:UsersManage:edit.html.twig")
     */
    public function createAction(Request $request) {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();

        $form = $this->createForm(UserFormType::class, $user, ['validation_groups'=> ['Registration']]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var $userManager UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);
            $url = $this->generateUrl('users_manage_show', ['id'=> $user->getId()]);
            $response = new RedirectResponse($url);
            return $response;
        }

        return array(
            'entity' => $user,
            'form' => $form->createView(),
        );
    }
}