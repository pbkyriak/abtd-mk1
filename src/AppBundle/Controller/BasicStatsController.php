<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ArticleQuestionAnswer;
use AppBundle\Entity\ArticleReview;
use AppBundle\Entity\QuestionMultiple;
use AppBundle\Form\FltArticleReviewType;
use AppBundle\Helper\FltQuestions;
use AppBundle\Report\DynamicRegistrators\QuestionsReports;
use AppBundle\Report\ReportFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;

/**
 * @Breadcrumb("Home", route="slx_metronic_homepage")
 * @Breadcrumb("Statistics" )
 * @CurrentMenuItem("basic_stats")
 */
class BasicStatsController extends AbstractAdminController {

    public function indexAction(Request $request, $rKey) {
        $this->actionPre($request);

        $rf = $this->get('AppBundle\Report\ReportFactory');
        $rds = $rf->getReportList();

        $rd = '';
        if(!empty($rKey)) {
            if(isset($rds[$rKey])) {
                $rd = $rKey;
            }
        }
        // show first report
        if(empty($rd)) {
            $rd = current(array_keys($rds));
        }
        $title = $rds[$rd];
        $rr = $rf->getReport($rd);
        $data = $rr->getData();
        $report = [
            'title' => $title,
            'id' => $rd,
            'type' => $rr->getChartType(),
            'data' => $rr->getData(),
        ];

        return $this->render(
            'AppBundle:BasicStats:index.html.twig',
            [
                'report' => $report,
                'report_list' => $rds,
            ]
        );
    }

}