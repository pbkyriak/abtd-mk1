<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Entity\ArticleComment;
use AppBundle\Events;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use AppBundle\Form\ArticleCommentFormType;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;

/**
 * Class ArticleCommentController
 */
class ArticleCommentController extends AbstractAdminController {

    public function commentFormAction(Request $request, $articleId) {
        $em = $this->getDoctrine();
        $article = $em->getRepository("AppBundle:Article")->find($articleId);
        list($comment, $commentForm) = $this->createNewCommentForm($article);

        return $this->render('AppBundle:ArticleComment:_comment_form.html.twig', [
            'article' => $article,
            'commentForm' => $commentForm->createView(),
        ]);
    }

    /**
     * @Breadcrumb("Home", route="home_page")
     * @Breadcrumb("Articles", route="articles")
     * @CurrentMenuItem("articles")
     * @param Request                  $request
     * @param                          $articleId
     * @param EventDispatcherInterface $eventDispatcher
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, $articleId,EventDispatcherInterface $eventDispatcher) {
        $em = $this->getDoctrine();
        $article = $em->getRepository("AppBundle:Article")->find($articleId);
        $this->get("apy_breadcrumb_trail")->add($article->getTitle());
        list($comment, $commentForm) = $this->createNewCommentForm($article);
        if ($request->isMethod('POST')) {
            $commentForm->handleRequest($request);
            if ($commentForm->isValid()) {
                $this->getDoctrine()
                     ->getManager()
                     ->persist($comment);
                $this->getDoctrine()
                     ->getManager()
                     ->flush($comment);
                $this->addFlash('info', 'app.article.comment_posted');
                $event = new GenericEvent($comment);
                $eventDispatcher->dispatch(Events::COMMENT_CREATED, $event);
                return $this->redirectToRoute('article_show', ['article_id' => $article->getId()]);
            }
        }
        return $this->render('AppBundle:ArticleComment:create_comment.html.twig', [
            'article' => $article,
            'form' => $commentForm->createView(),
        ]);

    }

    public function editAction(Request $request, $articleId, $commentId, EventDispatcherInterface $eventDispatcher) {
        $em = $this->getDoctrine();
        $article = $em->getRepository("AppBundle:Article")->find($articleId);
        $this->get("apy_breadcrumb_trail")->add($article->getTitle());
        $comment = $em->getRepository('AppBundle:ArticleComment')->find($commentId);
        $commentForm = $this->createForm(ArticleCommentFormType::class, $comment);
        if ($request->isMethod('POST')) {
            $commentForm->handleRequest($request);
            if ($commentForm->isValid()) {
                $this->getDoctrine()
                     ->getManager()
                     ->persist($comment);
                $this->getDoctrine()
                     ->getManager()
                     ->flush($comment);
                $this->addFlash('info', 'app.article.comment_posted');
                $event = new GenericEvent($comment);
                $eventDispatcher->dispatch(Events::COMMENT_UPDATED, $event);
                return $this->redirectToRoute('article_show', ['article_id' => $article->getId()]);
            }
        }
        return $this->render('AppBundle:ArticleComment:create_comment.html.twig', [
            'article' => $article,
            'comment' => $comment,
            'form' => $commentForm->createView(),
        ]);

    }
    private function createNewCommentForm($article) {
        $comment = new ArticleComment();
        $comment->setArticle($article);
        $comment->setCommenter($this->getLoggedUserEntity());
        $commentForm = $this->createForm(ArticleCommentFormType::class, $comment);
        return [$comment, $commentForm];
    }

    public function removeAction(Request $request, $articleId, $commentId,EventDispatcherInterface $eventDispatcher) {
        $em = $this->getDoctrine();
        $article = $em->getRepository("AppBundle:Article")->find($articleId);
        $comment = $em->getRepository("AppBundle:ArticleComment")->find($commentId);
        if($this->isGranted('delete', $comment)) {
            $this->getDoctrine()
                 ->getManager()
                 ->remove($comment);
            $this->getDoctrine()
                 ->getManager()
                 ->flush($comment);
            $this->addFlash('info', 'app.article.comment_posted');
            $event = new GenericEvent($article);
            $eventDispatcher->dispatch(Events::COMMENT_DELETED, $event);
        }
        return $this->redirectToRoute('article_show', ['article_id' => $article->getId()]);
    }

    public function latestAction(Request $request) {
        $comments = $this->getDoctrine()->getRepository('AppBundle:ArticleComment')->getLatest(5);
        return $this->render(
            'AppBundle:ArticleComment:latest.html.twig',
            ['comments' => $comments,]
        );
    }
}