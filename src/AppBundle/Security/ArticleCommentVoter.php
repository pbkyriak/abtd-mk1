<?php

namespace AppBundle\Security;

use AppBundle\Entity\ArticleComment;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ArticleCommentVoter extends Voter {

    const EDIT = 'edit';
    const DELETE = 'delete';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed  $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject) {
        if(!in_array($attribute, [self::EDIT,self::DELETE])) {
            return false;
        }
        if( !$subject instanceof ArticleComment) {
            return false;
        }
        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token) {

        if(!$subject instanceof ArticleComment) {
            return false;
        }

        $user = $token->getUser();
        if(!$user instanceof User) {
            return false;
        }

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        // owner can do anything on any status
        if($attribute==self::DELETE) {
            if($user==$subject->getCommenter() || $user==$subject->getArticle()->getOwner()) {
                return true;
            }
        }

        // owner can edit comment for 1 day after submission
        if($attribute==self::EDIT) {
            if($user==$subject->getCommenter()) {
                $diff = $subject->getCreatedAt()->diff(new \DateTime());
                if($diff->days<1) {
                    return true;
                }
            }
        }
        return false;
    }
}