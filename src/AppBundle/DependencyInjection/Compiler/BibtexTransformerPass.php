<?php
namespace AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class BibtexTransformerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // always first check if the primary service is defined
        if (!$container->has('transformer.bibtex_to_article')) {
            return;
        }

        $definition = $container->findDefinition('transformer.bibtex_to_article');

        // find all service IDs with the app.mail_transport tag
        $taggedServices = $container->findTaggedServiceIds('app.bibtex_transformer');

        foreach ($taggedServices as $id => $tags) {
            foreach($tags as $tag) {
                if(isset($tag['type']) ) {
                    $definition->addMethodCall('setTransformer', array($tag['type'], new Reference($id)));
                }
            }

        }
    }
}