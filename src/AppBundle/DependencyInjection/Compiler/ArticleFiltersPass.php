<?php

namespace AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class ArticleFiltersPass implements CompilerPassInterface {
    public function process(ContainerBuilder $container) {
        $this->processFilterTags($container);
        $this->processFormTags($container);

    }

    public function processFilterTags(ContainerBuilder $container) {
        // always first check if the primary service is defined
        if (!$container->has('AppBundle\ArticleFilters\ArticleFilters')) {
            return;
        }

        $definition = $container->findDefinition('AppBundle\ArticleFilters\ArticleFilters');

        // find all service IDs with the app.mail_transport tag
        $taggedServices = $container->findTaggedServiceIds('app.article_filter');
        foreach ($taggedServices as $id => $tags) {
            foreach($tags as $tag) {
                if(isset($tag['type']) ) {
                    $definition->addMethodCall('addFilter', array($tag['type'], new Reference($id)));
                }
            }
        }
    }

    public function processFormTags(ContainerBuilder $container) {
        // always first check if the primary service is defined
        if (!$container->has('AppBundle\Form\FltArticleReviewType')) {
            return;
        }

        $definition = $container->findDefinition('AppBundle\Form\FltArticleReviewType');

        // find all service IDs with the app.mail_transport tag
        $taggedServices = $container->findTaggedServiceIds('app.article_filter_form');
        foreach ($taggedServices as $id => $tags) {
            foreach($tags as $tag) {
                if(isset($tag['type']) ) {
                    $definition->addMethodCall('addFormSubscriber', array($tag['type'], new Reference($id)));
                }
            }

        }
    }


}