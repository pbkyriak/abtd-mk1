<?php

namespace AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class QuestionAnswerFieldsPass  implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // always first check if the primary service is defined
        if (!$container->has('AppBundle\Form\ReviewQuestionType')) {
            return;
        }

        $definition = $container->findDefinition('AppBundle\Form\ReviewQuestionType');

        $definition0 = $container->findDefinition('app.question_types_manager');
        $calls = $definition0->getMethodCalls();
        foreach($calls as $call) {
            if($call[0]=='addReviewQuestionFieldListener') {
                $definition->addMethodCall('addFormSubscriber', [$call[1][0]]);
            }
        }

    }
}