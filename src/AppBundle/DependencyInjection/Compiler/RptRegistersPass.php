<?php

namespace AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class RptRegistersPass implements CompilerPassInterface {

    public function process(ContainerBuilder $container) {
        $this->processRegisters($container);
        $this->processReports($container);
    }

    private function processRegisters(ContainerBuilder $container) {
        // always first check if the primary service is defined
        if (!$container->has('AppBundle\Report\ReportFactory')) {
            return;
        }

        $definition = $container->findDefinition('AppBundle\Report\ReportFactory');

        // find all service IDs with the app.mail_transport tag
        $taggedServices = $container->findTaggedServiceIds('app.report_register');
        foreach ($taggedServices as $id => $tags) {
            foreach($tags as $tag) {
                $definition->addMethodCall('addDynReports', array(new Reference($id)));
            }
        }

    }

    private function processReports(ContainerBuilder $container) {
        // always first check if the primary service is defined
        if (!$container->has('AppBundle\Report\ReportFactory')) {
            return;
        }

        $definition = $container->findDefinition('AppBundle\Report\ReportFactory');

        // find all service IDs with the app.mail_transport tag
        $taggedServices = $container->findTaggedServiceIds('app.report');
        foreach ($taggedServices as $id => $tags) {
            foreach($tags as $tag) {
                if(isset($tag['id']) ) {
                    $definition->addMethodCall('addReport', array($tag['id'], new Reference($id), ['title'=>'']));
                }
            }
        }

    }

}