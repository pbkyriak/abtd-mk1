<?php

namespace AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class EntityMatcherPass  implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // always first check if the primary service is defined
        if (!$container->has('matcher.manager')) {
            return;
        }

        $definition = $container->findDefinition('matcher.manager');

        // find all service IDs with the app.mail_transport tag
        $taggedServices = $container->findTaggedServiceIds('app.entity_matcher');

        foreach ($taggedServices as $id => $tags) {
            foreach($tags as $tag) {
                if(isset($tag['type']) ) {
                    $definition->addMethodCall('setMatcher', array($tag['type'], new Reference($id)));
                }
            }

        }
    }
}