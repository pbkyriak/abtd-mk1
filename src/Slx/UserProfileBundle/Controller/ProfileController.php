<?php

namespace Slx\UserProfileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Slx\UserBundle\Form\MyProfileType;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;

/**
 * Description of Profile
 * @Breadcrumb("slx_metronic.general.home", route="slx_metronic_homepage")
 * @Breadcrumb("users.profile.edit", route="slx_user_profile_edit")
 * 
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Aug 23, 2016
 */
class ProfileController extends Controller {

    public function indexAction() {
        return $this->render('SlxUserProfileBundle:Default:index.html.twig');
    }

    /**
     * 
     */
    public function editAction(Request $request) {
        $id = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SlxUserBundle:User')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('users.user.not_found');
        }
        $form = $this->createForm(MyProfileType::class, $entity);
        
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('info', 'users.profile.profile_updated');
                return $this->redirect($this->generateUrl('slx_metronic_homepage'));
            }
        }
        
        return $this->render('SlxUserProfileBundle:Profile:edit.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
        
    }
}
