<?php

namespace Slx\UserProfileBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * UserProfile form to be embeded to User.
 * This form is for adminstration pages. Not for user to edit their profile.
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Aug 23, 2016
 */
class UserProfileType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('firstname', TextType::class, ['required'=> false, 'label'=>'users.profile.firstname'])
                ->add('lastname', TextType::class, ['required'=> false, 'label'=>'users.profile.lastname'])
                ->add('mobile', TextType::class, ['required'=> false, 'label'=>'users.profile.mobile'])
                ->add('phone', TextType::class, ['required'=> false, 'label'=>'users.profile.phone'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Slx\UserProfileBundle\Entity\UserProfile'
        ));
    }

    public function getName() {
        return 'slx_userprofilebundle_userprofiletype';
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'userprofile';
    }
}
