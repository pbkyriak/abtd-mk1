#SlxUserProfile Bundle

This is a user profile bundle, the companion bundle of SlxUserBundle (both must be installed). 
Holds only basic user profile table and form. The role of this bundle to have a place to modify and add needed per case extra profile fields.

Modify Resources/doctrine/UserProfile.orm.yml to add extra fields in profile table.

Modify accordingly Form/UserProfileType.php (add the extra fields).

Modify Resources/views/Components/user_profile.html.twig to show the new fields (this template currently is only used by the administrator pages)

And if needed modify Resources/views/Profile/edit.html.twig (this is the template showing the form)
