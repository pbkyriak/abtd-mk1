<?php
namespace Slx\UserProfileBundle\Entity;
/**
 * Description of UserProfile
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Aug 23, 2016
 */
class UserProfile {
    private $id;
    private $firstname;
    private $lastname;
    private $phone;
    private $mobile;
    private $user;
    
    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }
    
    function getFirstname() {
        return $this->firstname;
    }

    function getLastname() {
        return $this->lastname;
    }

    function getPhone() {
        return $this->phone;
    }

    function getMobile() {
        return $this->mobile;
    }

    function getUser() {
        return $this->user;
    }

    function setFirstname($firstName) {
        $this->firstname = $firstName;
        return $this;
    }

    function setLastname($lastName) {
        $this->lastname = $lastName;
        return $this;
    }

    function setPhone($phone) {
        $this->phone = $phone;
        return $this;
    }

    function setMobile($mobile) {
        $this->mobile = $mobile;
        return $this;
    }

    function setUser($user) {
        $this->user = $user;
        return $this;
    }

    
}
