<?php

namespace Slx\LockedEntityTypeBundle\Form\ViewTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Exception\InvalidConfigurationException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;


class LockedEntityViewTransformer implements DataTransformerInterface  {
    protected $class;
    /**
     * @var string
     */
    protected $choice_label;

    /**
     * @var string
     */
    protected $choice_value;

    /**
     * @var EntityManager
     */
    protected $em;

    /** @var EntityRepository */
    protected $repository;

    public function __construct(ManagerRegistry $registry, $omName, $class, $choice_label, $choice_value) {
        $this->class = $class;
        $this->choice_label = $choice_label;
        $this->choice_value = $choice_value;
        $this->em = $this->getObjectManager($registry, $omName);
        $this->repository = $this->getObjectRepository($this->em, $this->class);
    }

    /**
     * ID comes in, array with id and title goes out
     * @param mixed $value
     * @return array
     */
    public function transform($value)
    {
        if(null===$value) {
            return null;
        }
        // get entity from db to get label
        $entity = $this->repository->findOneBy([$this->choice_value => $value]);
        if (null === $entity) {
            throw new TransformationFailedException(sprintf('Can\'t find entity of class "%s" with property "%s" = "%s".', $this->class, $this->choice_value, $value));
        }
        $label = '';
        if(is_callable($this->choice_label)) {
            $label = call_user_func_array($this->choice_label,[$entity]);
        }
        else {
            $accessor = PropertyAccess::createPropertyAccessor();
            $label = $accessor->getValue($entity, $this->choice_label);
        }
        $object = [
            'id' => $value,
            'title' => $label,
        ];
        return $object;
    }

    /**
     * ID comes in ID gets out
     * @param mixed $number
     * @return mixed|null
     */
    public function reverseTransform($number)
    {
        if (!$number) {
            return null;
        }
        return $number;
    }

    /**
     * @param ManagerRegistry $registry
     * @param ObjectManager|string $omName
     * @return ObjectManager
     */
    private function getObjectManager(ManagerRegistry $registry, $omName)
    {
        if ($omName instanceof ObjectManager) {
            return $omName;
        }

        $omName = (string) $omName;
        if ($om = $registry->getManager($omName)) {
            return $om;
        }

        throw new InvalidConfigurationException(sprintf('Doctrine Manager named "%s" does not exist.', $omName));
    }

    /**
     * @param ObjectManager $om
     * @param string $class
     * @return ObjectRepository
     */
    private function getObjectRepository(ObjectManager $om, $class)
    {
        if ($repo = $om->getRepository($class)) {
            return $repo;
        }

        throw new InvalidConfigurationException(sprintf('Repository for class "%s" does not exist.', $class));
    }

}