<?php

namespace Slx\LockedEntityTypeBundle\Form\Type;

use Slx\LockedEntityTypeBundle\Form\ViewTransformer\LockedEntityViewTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\Form\Exception\RuntimeException;

/**
 * Class LockedEntityType
 * @package Slx\LockedEntityTypeBundle\Form\Type
 *
 */
class LockedEntityType extends AbstractType {

    /**
     * @var ManagerRegistry
     */
    protected $registry;

    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry) {
        $this->registry = $registry;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $viewTransformer = new LockedEntityViewTransformer(
            $this->registry,
            $options['em'],
            $options['class'],
            $options['choice_label'],
            $options['choice_value']
        );
        $builder->addViewTransformer($viewTransformer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver
            ->setRequired(['class'])
            ->setDefaults(
                [
                    'em' => null,
                    'data_class' => null,
                    'label' => '',
                    'choice_value' => 'id',
                    'choice_label' => 'title',
                    'om' => 'default',
                    'widget' => 'lockedentity',
                    'compound' => false,
                ]
            );

        $emNormalizer = function (Options $options, $em) {
            /* @var ManagerRegistry $registry */
            if (null !== $em) {
                if ($em instanceof ObjectManager) {
                    return $em;
                }

                return $this->registry->getManager($em);
            }

            $em = $this->registry->getManagerForClass($options['class']);

            if (null === $em) {
                throw new RuntimeException(sprintf(
                                               'Class "%s" seems not to be a managed Doctrine entity. '.
                                               'Did you forget to map it?',
                                               $options['class']
                                           ));
            }

            return $em;
        };

        $resolver->setNormalizer('em', $emNormalizer);

        $resolver
            ->setAllowedTypes('invalid_message', ['null', 'string'])
            ->setAllowedTypes('choice_label', ['string', 'callable'])
            ->setAllowedTypes('choice_value', ['string'])
            ->setAllowedTypes('label', ['string'])
            ->setAllowedTypes('em', ['null', 'string', 'Doctrine\Common\Persistence\ObjectManager']);
    }

    public function getName() {
        return 'lockedentity';
    }


}