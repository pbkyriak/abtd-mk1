<?php

namespace Slx\LockedEntityTypeBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class FormThemePass implements CompilerPassInterface {
    public function process(ContainerBuilder $container) {

        $resources = [];
        if ($container->hasParameter('twig.form.resources')) {
            $resources = $container->getParameter('twig.form.resources');
        }
        $resources[] = 'SlxLockedEntityTypeBundle:Form:widgets.html.twig';
        $container->setParameter('twig.form.resources', $resources);

    }
}
