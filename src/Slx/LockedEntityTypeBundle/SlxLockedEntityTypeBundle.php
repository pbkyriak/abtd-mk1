<?php

namespace Slx\LockedEntityTypeBundle;

use Slx\LockedEntityTypeBundle\DependencyInjection\Compiler\FormThemePass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SlxLockedEntityTypeBundle extends Bundle {
    public function build(ContainerBuilder $container) {
        $container->addCompilerPass(new FormThemePass());
    }

}
