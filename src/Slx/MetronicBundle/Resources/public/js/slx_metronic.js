
(function (core) {
    $.extend(core, {
    
        // initialize date mask inputs
        handleInputMasks: function() {
            Inputmask({"mask": "d/m/y","autoUnmask": true}).mask(".hasDatepicker");
        },

        /* initializes modal confirm links */
        initModalConfirm: function () {
            $('.modal-confirm').each(function() {
                var msg = $(this).data('confirm-message');
                var ref = $(this).attr('href');
                $(this).click(function() {
                    bootbox.dialog({
                      message: msg,
                      title: SlxMetronic.trans.confirm,
                      buttons: {
                        cancel: {
                          label: SlxMetronic.trans.cancel,
                          className: "btn-default",
                          callback: function() {
                            
                          }
                        },
                        danger: {
                          label: SlxMetronic.trans.ok,
                          className: "btn-primary",
                          callback: function() {
                            window.location = ref;
                          }
                        }
                      }
                    });
                    return false;
                });
            });
        },


        /* Object with collection edit methods */
        collectionEdit: {
            
            /* adds new row to collections */
            addRow: function(inBtn) {
                var btn = $(inBtn);
                var collectionHolder = btn.closest('div.collectionholder');
                // Get the data-prototype explained earlier
                var prototype = collectionHolder.data('prototype');

                // get the new index
                var index = collectionHolder.data('index');

                // Replace '__name__' in the prototype's HTML to
                // instead be a number based on how many items we have
                var newForm = prototype.replace(/__name__/g, index);

                // increase the index with one for the next item
                collectionHolder.data('index', index + 1);
                $('table tbody', collectionHolder).append(newForm);                
            },
    
            /* removes row for the collection, if last row is removed then adds a new one */
            removeRow: function(inBtn) {
                var btn = $(inBtn);
                var collectionHolder = btn.closest('div.collectionholder');
                var addBtn = collectionHolder.find('a.add');
                btn.closest('tr').remove();
                if ($('table tbody tr', collectionHolder).length === 0) {
                    SlxMetronic.collectionEdit.addRow(addBtn);
                }
            }
        },
        
        /* initializes collection edit */
        initCollectionEdit: function () {
            $('div.collectionholder').each(function() {
                var collectionHolder = $(this);
                collectionHolder.data('index', collectionHolder.find('tr').length);
                $( collectionHolder).on('click','a.add', function (event) {
                    event.preventDefault();
                    SlxMetronic.collectionEdit.addRow(event.target);
                });
                
                $(collectionHolder).on('click', 'a.remove', function (event) {
                    event.preventDefault();
                    SlxMetronic.collectionEdit.removeRow(event.target);
                });
            });
        },
                
        /* ---------------------------------------------------------------------
         * Select2 functions 
         */ 
        
        // format results 
        select2ResultsFormatFunc : function(entry) {
            if (entry.loading) return entry.text;
            var markup = "<div>"+entry.text+"</div>";
            return markup;
        },

        // format item
        select2SelectionFormatFunc : function(entry) {
            return entry.text;
        },

        // initialize select2 elements
        initSelect2Func : function() {
            selects = $('.select2obj').each(function() {
                var obj = $(this);
                if (0 == obj.data('inited')) {
                    var urlList = obj.data('list-source');
                    obj.data('inited', 1);
                    obj.select2({
                        placeholder: SlxMetronic.trans.pleaseselect,
                        width: null,
                        ajax: {
                            url: urlList,
                            dataType: 'json',
                            type: 'GET',
                            delay: 250,
                            data: function(params) {
                                return {
                                    q: params.term, // search term
                                    page: params.page
                                };
                            },
                            processResults: function (data) {
                                return {
                                    results: data
                                };
                            },
                            cache: true
                        },
                        escapeMarkup: function(markup) {
                            return markup;
                        }, // let our custom formatter work
                        minimumInputLength: 1,
                        templateResult: SlxMetronic.select2ResultsFormatFunc,
                        templateSelection: SlxMetronic.select2SelectionFormatFunc
                    });
                    
                    
                }
            });

        },
        
        // call all initializations
        init: function() {
            SlxMetronic.handleInputMasks();
            SlxMetronic.initModalConfirm();
            SlxMetronic.initCollectionEdit();
            SlxMetronic.initSelect2Func();
        }
    
    });
}(SlxMetronic));

/* on document ready initialize SlxMetronic staf */
$(document).ready(function(){
    SlxMetronic.init();
});

