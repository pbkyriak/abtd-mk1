#Slx Metronic Bundle

This bundle depends and works along with SlxUserBundle and SlxUserProfileBundle. All must be installed!

## Setup 

Add to your project's composer.json the following:

```

"apy/breadcrumbtrail-bundle": "dev-master",
"knplabs/knp-menu-bundle": "v1.1.2",
"knplabs/knp-paginator-bundle": "dev-master",
"ornicar/gravatar-bundle" : "~1.0",
"dmishh/settings-bundle": "2.0.*@dev"

```

Enable the bundles in your app/AppKernel.php.

```
            new APY\BreadcrumbTrailBundle\APYBreadcrumbTrailBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Ornicar\GravatarBundle\OrnicarGravatarBundle(),
            new Slx\MetronicBundle\SlxMetronicBundle(),
            new Slx\UserBundle\SlxUserBundle(),
```
Add to your app/config/config.yml file the following:

```
slx_metronic:
    app_title: SlxMetronic 4.6
    app_copyright: 2016
    app_company: Salix.gr
    app_company_url: http://salix.gr
    color_theme: blue|darkblue|default|gray|light|light2 (selects the css in admin_styles.html.twig line 16)
    admin_menu_alias: main

apy_breadcrumb_trail:
    template: SlxMetronicBundle:Components:apy_breadcrumbtrail.html.twig
    
knp_menu:
    twig:
        template: SlxMetronicBundle:Components:knp_menu.html.twig
    templating: false
    default_renderer: twig

knp_paginator:
    page_range: 5                      # default page range used in pagination control
    default_options:
        page_name: page                # page query parameter name
        sort_field_name: sort          # sort field query parameter name
        sort_direction_name: direction # sort direction query parameter name
        distinct: true                 # ensure distinct results, useful when ORM queries are using GROUP BY statements
    template:
        pagination: SlxMetronicBundle:Components:metronic_pagination.html.twig     # sliding pagination controls template
        sortable: KnpPaginatorBundle:Pagination:sortable_link.html.twig # sort link template

ornicar_gravatar:
    rating: g
    size: 29
    default: mm

dmishh_settings:
    template: SlxMetronicBundle:Components:settings_manage.html.twig
    settings:
        company_name:
            type: Symfony\Component\Form\Extension\Core\Type\TextType

```

On dmishh_settings you have to work on!! Configure the settings you need!! Read https://github.com/dmishh/SettingsBundle


Also in the application's config.yml you must add form theming parameters as in the following:

```
# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"
    form_themes:
        - 'SlxMetronicBundle:Form:fields_horizontal.html.twig'
        - 'SlxMetronicBundle:Form:collection.html.twig'
```

Just add the form_themes part in the twig section.

Then you need to setup your menu builder. At your admin bundle create a class like:

```
// file: src/AppBundle/Menu/MenuBuilder.php

namespace AppBundle\Menu;

use Slx\MetronicBundle\Menu\AbstractMenuBuilder;

class MenuBuilder extends AbstractMenuBuilder {
    
    public function createMainMenu(array $options)
    {
        //------------------------
        $isRAdmin = $this->securityContext->isGranted(array('ROLE_RADMIN'));
        $isAdmin = $this->securityContext->isGranted(array('ROLE_ADMIN'));
        $isSuperAdmin = $this->securityContext->isGranted(array('ROLE_SUPER_ADMIN'));
        //-----------------------
        $menu = $this->factory->createItem('root');
        $menu->addChild('Home', array('route' => 'slx_metronic_homepage'))->setAttribute('icon', 'fa-home');
        $ent = $menu->addChild('Καταχώρηση')->setAttribute('icon','fa-stack-overflow');
        $ent->addChild('Ψήφων', array('route'=>'slx_metronic_2'))->setAttribute('icon','fa-plug');
        $ent->addChild('Σταυρών', array('route'=>'slx_metronic_3'))->setAttribute('icon','fa-plug');
        $ent = $menu->addChild('Καταχώρηση2')->setAttribute('icon','fa-stack-overflow');
        $ent->addChild('One', array('route'=>'slx_metronic_4'))->setAttribute('icon','fa-plug');
        $ent->addChild('Two', array('route'=>'slx_metronic_5'))->setAttribute('icon','fa-plug');
        
        if($this->currentItemRouteName ) {
            $this->setCurrentMenuItem($menu, $this->currentItemRouteName);
        }
        return $menu;
    }
}

```

In the services.yml add the menu builder service:

```
    metronic_admin.menu_builder:
        class: AppBundle\Menu\MenuBuilder
        calls:
            - [setFactory, ['@knp_menu.factory']]
            - [setRouter, ['@router']]
            - [setSecurityContext, ['@security.authorization_checker']]
        tags:
            - { name: knp_menu.menu_builder, method: createMainMenu, alias: main }
```

Notice the alias, 'main' that same alias is the configuration variable in the 
config.yml shown earlier slx_metronic.admin_menu_alias. That will be admin panel's side menu.


##Notes

if you want translated string available to javascript edit Resources/views/admin_scripts.html.twig and at the end there is a place to add them.
Then you can access them in your javascripts like SlxMetronic.trans.my_text_variable