# ArticleSelectorType - Select2 with ajax

In your forms you can use select2 with ajax, the form type is named ArticleSelectorType.

In your form build code you added like:

```

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('note', TextType::class, array('label'=>'gsprod.color_code.code'))
            ->add(
                'user', 
                ArticleSelectorType::class, 
                array(
                    'label' => 'users.user.user',
                    'required'=>true,
                    'list_route' => 'users_json_list',
                    'entity' => 'SlxUserBundle:User',
                    'row_type' => 'compound',
                    'add_clear_btn' => true,
                )
            )
            ->add('save', SubmitType::class, array('label'=>'slx_metronic.general.save','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', SubmitType::class, array('label'=>'slx_metronic.general.save_and_add','attr'=>array('class'=>'btn blue')))
        ;
    }

```

Special parameters: 

- **entity** (required) The class of your entity (e.g. AppBundle:Category). 

- **list_route** (required) the route of the controller returning the json list for the component

- **list_route_params** (optional) (not in the example) any additional parameters needed for the ajax call to get the list of options.     
    For example is you need to send cId=2 also, then set ``` 'list_route_params' => array('cid' => 2), ```

- **add_clear_btn** (optional) if true the user can clear their selection

Your entity MUST implement __toString method.

The json list is implemented as in the next example:

```

    /**
     * @Route("/test/myjson/user/list", name="users_json_list")
     */
    public function jsonListAction(Request $request)
    {
        $q = str_replace('*', '%', $request->get('q'));

        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('SlxUserBundle:User')
            ->createQueryBuilder('a')
            ->where('a.username like :code')
            ;

        $entities = $qb
            ->setParameter('code', $q)
            ->getQuery()
            ->getResult();
        $results = array();
        if ($entities) {
            foreach ($entities as $entity) {
                $results[] = array('id' => $entity->getId(), 'text' => sprintf("%s - %s",
                        $entity->getUsername(), $entity->getEmail()));
            }
        }
        return $this->json($results);
    }

```

The response is an array of items with id and text fields.

