<?php

namespace Slx\MetronicBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class SlxMetronicExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
        /* any configuration set in config.yml slx_metronic section has to be added here so they are available to the container */
        $container->setParameter('slx_metronic.app_title', $config['app_title']);
        $container->setParameter('slx_metronic.app_copyright', $config['app_copyright']);
        $container->setParameter('slx_metronic.app_company', $config['app_company']);
        $container->setParameter('slx_metronic.app_company_url', $config['app_company_url']);
        $container->setParameter('slx_metronic.color_theme', $config['color_theme']);
        $container->setParameter('slx_metronic.admin_menu_alias', $config['admin_menu_alias']);
        
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}
