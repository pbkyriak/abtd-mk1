<?php

namespace Slx\MetronicBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('slx_metronic');

        $rootNode->children()
                 ->scalarNode('color_theme')->defaultValue('blue')->end()
                 ->scalarNode('app_title')->defaultValue('SlxMetronic')->end()
                 ->scalarNode('app_copyright')->defaultValue('2016')->end()
                 ->scalarNode('app_company')->defaultValue('Salix.gr')->end()
                 ->scalarNode('app_company_url')->defaultValue('http://salix.me')->end()
                 ->scalarNode('admin_menu_alias')->defaultValue('main')->end()
                 ->end();
        
        return $treeBuilder;
    }
}
