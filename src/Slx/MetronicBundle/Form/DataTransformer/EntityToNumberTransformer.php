<?php

namespace Slx\MetronicBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Description of EntityToNumberTransformer
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Aug 24, 2016
 */
class EntityToNumberTransformer implements DataTransformerInterface {
    
    /**
     * @var ObjectManager
     */
    private $om;
    private $entityClass;
    
    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om, $entityClass)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
    }

    /**
     * Transforms an object to a string (number).
     *
     * @param  $entity|null $issue
     * @return string
     */
    public function transform($entity)
    {
        //echo 'transform';die(1);
        $out = "";
        if (null === $entity) {
            $out = '';
        }
        elseif( method_exists($entity, 'getId') ) {

            $out = $entity->getId();
        }
        return $out;
    }

    /**
     * Transforms a string (number) to an object (DyeStepRecipie).
     *
     * @param  string $number
     *
     * @return object|null
     *
     * @throws TransformationFailedException if object (article) is not found.
     */
    public function reverseTransform($number)
    {
        if (!$number) {
            return null;
        }
        $repo = $this->om->getRepository($this->entityClass);
        if( $repo ) {
            $article = $repo->findOneBy(array('id' => $number));
        }
        if (null === $article) {
            throw new TransformationFailedException(sprintf('An entity with id "%s" does not exist!', $number ));
        }

        return $article;
    }
    
}
