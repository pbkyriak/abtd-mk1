<?php

namespace Slx\MetronicBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Slx\MetronicBundle\Form\DataTransformer\EntityToNumberTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Description of Select2Type
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ArticleSelectorType extends AbstractType
{

    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new EntityToNumberTransformer($this->om, $options['entity']);
        $builder->addModelTransformer($transformer);
    }

    public function buildView(FormView $view, FormInterface $form, array $options) {
        parent::buildView($view, $form, $options);
        
        $selObject = $form->getData(); // if there is an object selected, use it to get it string representation to show for selected value
        $newVars = [
            'list_route'        => $options['list_route'],
            'list_route_params' => isset($options['list_route_params']) ? $options['list_route_params'] : array(),
            'row_type'          => isset($options['row_type']) ? $options['row_type'] : 'hidden',
            'add_clear_btn'     => isset($options['add_clear_btn']) ? $options['add_clear_btn'] : false,            
            'value_text'        => is_object($selObject) ? (string)$selObject : ''
        ];
        $view->vars = array_replace($view->vars, $newVars);
        
    }

    public function getParent()
    {
        return TextType::class;
    }

    public function getName()
    {
        return 'article_selector';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'invalid_message' => 'Selected entity does not exist',
            'list_route_params' => array(),
            'row_type' => '',
            'add_clear_btn' => true,
            'item_route' => '',
            'entity' => '',
        ));
        $resolver->setRequired(array('list_route', 'entity'));
    }

}

