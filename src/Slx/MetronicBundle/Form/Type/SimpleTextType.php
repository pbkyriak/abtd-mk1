<?php
/**
 * @copyright 2017 Kartpay.com.
 * @author Panos <panos@kartpay.com>
 * Date: 3/12/2017
 * Time: 8:03 μμ
 */

namespace Slx\MetronicBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class SimpleTextType extends AbstractType {

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
                                   'label' => "opoi",
                                    'mapped' => false,
                               ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['valid'] = $valid = !$form->isSubmitted() || $form->isValid();
        $view->vars['label'] = $options['label'];
    }

    public function getName()
    {
        return 'simple_text';
    }
}