<?php

namespace Slx\MetronicBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

/**
 * Description of TabType
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Jul 7, 2017
 */
class TabType extends AbstractType {

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'icon' => null,
            'error_icon' => 'remove-sign',
            'disabled' => false,
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['valid'] = $valid = !$form->isSubmitted() || $form->isValid();
        $view->vars['icon'] = $valid ? $options['icon'] : $options['error_icon'];
        $view->vars['tab_active'] = false;
        $view->vars['disabled'] = $options['disabled'];

        $view->parent->vars['tabbed'] = true;
    }

}
