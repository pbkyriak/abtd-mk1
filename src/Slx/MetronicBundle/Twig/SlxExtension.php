<?php

namespace Slx\MetronicBundle\Twig;

/**
 * Description of SlxExtension
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class SlxExtension extends \Twig_Extension {

    /**
     * {@inheritDoc}
     */
    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('paginator_header_class', array($this, 'paginatorHeaderClassFilter')),
            new \Twig_SimpleFilter('boolToText', array($this, 'boolToText')),
            new \Twig_SimpleFilter('percTo', array($this, 'percTo')),
        );
    }

    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('form_help', null, array(
                'node_class' => 'Symfony\Bridge\Twig\Node\SearchAndRenderBlockNode',
                'is_safe' => array('html'),
                    )),
            new \Twig_SimpleFunction('form_tabs', null, array(
                'node_class' => 'Symfony\Bridge\Twig\Node\SearchAndRenderBlockNode',
                'is_safe' => array('html'),
                    )),
        );
    }

    public function paginatorHeaderClassFilter($pagination, $fieldName) {
        $class = 'sorting';
        $params = $pagination->getParams();
        $direction = '';
        if (in_array('asc', $params))
            $direction = 'asc';
        if (in_array('desc', $params))
            $direction = 'desc';
        if ($pagination->isSorted($fieldName))
            $class = 'sorting_' . $direction;
        return $class;
    }

    public function boolToText($bool) {
        if ($bool)
            return 'slx_metronic.general.yes';
        else
            return 'slx_metronic.general.no';
    }

    public function percTo($part, $whole) {
        if ($whole) {
            return round(($part / $whole * 100), 1);
        } else {
            return 0;
        }
    }

    public function getName() {
        return 'slx_extension';
    }

}
