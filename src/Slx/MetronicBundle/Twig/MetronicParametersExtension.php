<?php
namespace Slx\MetronicBundle\Twig;
use Symfony\Component\DependencyInjection\ContainerInterface;
/**
 * Adds globals to twig with values comming from config.yml
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Aug 20, 2016
 */
class MetronicParametersExtension extends \Twig_Extension {
    protected $container, $notify;

    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
        
        $this->container->get('twig')->addGlobal('app_title', $this->container->getParameter('slx_metronic.app_title'));
        $this->container->get('twig')->addGlobal('color_theme', $this->container->getParameter('slx_metronic.color_theme'));
        $this->container->get('twig')->addGlobal('app_copyright', $this->container->getParameter('slx_metronic.app_copyright'));
        $this->container->get('twig')->addGlobal('app_company', $this->container->getParameter('slx_metronic.app_company'));
        $this->container->get('twig')->addGlobal('app_company_url', $this->container->getParameter('slx_metronic.app_company_url'));
    }
    
    public function getName() {
        return 'metronic_parameters';
    }
}
