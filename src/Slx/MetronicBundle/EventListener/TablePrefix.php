<?php
/**
 * @copyright 2017 Kartpay.com.
 * @author Panos <panos@kartpay.com>
 * Date: 15/10/2017
 * Time: 3:35 μμ
 */

namespace Slx\MetronicBundle\EventListener;

use \Doctrine\ORM\Event\LoadClassMetadataEventArgs;

class TablePrefix implements \Doctrine\Common\EventSubscriber {
    protected $prefix = '';

    public function __construct($prefix) {
        $this->prefix = (string)$prefix;
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs) {
        $classMetadata = $eventArgs->getClassMetadata();

        if (!$classMetadata->isInheritanceTypeSingleTable() || $classMetadata->getName() === $classMetadata->rootEntityName) {
            $classMetadata->setPrimaryTable([
                                                'name' => $this->prefix . $classMetadata->getTableName()
                                            ]);
        }

        foreach ($classMetadata->getAssociationMappings() as $fieldName => $mapping) {
            if ($mapping['type'] == \Doctrine\ORM\Mapping\ClassMetadataInfo::MANY_TO_MANY && $mapping['isOwningSide']) {
                $mappedTableName = $mapping['joinTable']['name'];
                $classMetadata->associationMappings[$fieldName]['joinTable']['name'] = $this->prefix . $mappedTableName;
            }
        }
    }

    public function getSubscribedEvents() {
        return array('loadClassMetadata');
    }
}