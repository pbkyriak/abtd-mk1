<?php

namespace Slx\MetronicBundle\Menu;

/**
 * Description of MenuBuilderInterface
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
interface MenuBuilderInterface
{
    public function setCurrentMenuItemRouteName($routeName);
}
