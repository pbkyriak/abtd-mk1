<?php

namespace Slx\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Slx\UserProfileBundle\Form\UserProfileType;

/**
 * This is the form for users to edit their profile.
 * Embeds UserProfileType form for your custom fields.
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Aug 23, 2016
 */
class MyProfileType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('email')
                ->add('profile', UserProfileType::class, ['label' => 'users.profile.profile', 'required' => false])
                ->add('save', SubmitType::class, array('label' => 'slx_metronic.general.save', 'attr' => array('class' => 'btn blue')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Slx\UserBundle\Entity\User'
        ));
    }

    public function getName() {
        return 'slx_userbundle_myprofiletype';
    }

}
