<?php

namespace Slx\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Slx\UserProfileBundle\Form\UserProfileType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('email')
            ->add('isActive', null, array('required'=>false))
            ->add('userType', ChoiceType::class, array('label'=>'users.user.userType', 'choices'=>array('slx_metronic.general.user_type_0'=>0, 'slx_metronic.general.user_type_1'=>1, 'slx_metronic.general.user_type_2'=>2)))
            ->add('roles', null, array('label'=>'users.user.roles', 'multiple'=>true))
            ->add('profile', UserProfileType::class, ['label'=>'users.profile.profile', 'required'=>false])
            ->add('save', SubmitType::class, array('label'=>'slx_metronic.general.save','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', SubmitType::class, array('label'=>'slx_metronic.general.save_and_add','attr'=>array('class'=>'btn blue')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Slx\UserBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'slx_userbundle_usertype';
    }
}
