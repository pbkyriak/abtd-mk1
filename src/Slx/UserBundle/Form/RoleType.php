<?php

namespace Slx\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null,['label'=>'users.user.title'])
            ->add('role',null,['label'=>'users.user.role_token'])
            ->add('save', SubmitType::class, array('label'=>'slx_metronic.general.save','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', SubmitType::class, array('label'=>'slx_metronic.general.save_and_add','attr'=>array('class'=>'btn blue')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Slx\UserBundle\Entity\Role'
        ));
    }

    public function getName()
    {
        return 'slx_userbundle_roletype';
    }
}
