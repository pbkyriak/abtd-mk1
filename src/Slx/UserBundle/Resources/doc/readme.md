#Users bundle

This is a simple but easy to use user bundle. The companion bundle is SlxUserProfileBundle 
that handles profile info (both must be installed). .

##Setup

Copy-paste bundle in src/Slx and enable it in your app/AppKernel 

```
new Slx\UserBundle\SlxUserBundle(),
new Slx\UserBundle\SlxUserProfileBundle(),
```

Add to app/config/routing.yml the next in order to import bundle's routes

```
slx_user:
    resource: "@SlxUserBundle/Resources/config/routing.yml"
    prefix:   /
```

Then edit app/config/security.yml and make it like the following.

```
security:
    encoders:
        Slx\UserBundle\Security\User\SlxUser: md5

    role_hierarchy:
        ROLE_ADMIN:       ROLE_USER
        ROLE_SUPER_ADMIN: [ROLE_USER, ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]

    providers:
        slx_users:
            id: slx_user_provider
        
    firewalls:
        dev:
            pattern:  ^/(_(profiler|wdt)|css|images|js)/
            security: false
            
        login_firewall:
            pattern:    ^/login$
            anonymous:  ~
            
        admin_area:
            pattern: ^/
            form_login:
                login_path: /login
                check_path: /login_check
                use_referer: true
                default_target_path: /
            logout:
                path: /logout
                target: /login
                invalidate_session: false
            remember_me:
              key: "%secret%"
              lifetime: 31536000
              path: /
              domain: ~

    access_control:
        - { path: ^/login, roles: IS_AUTHENTICATED_ANONYMOUSLY }
        
        - { path: ^/machineType, roles: [ROLE_SUPER_ADMIN] }
        - { path: ^/calcType, roles: [ROLE_SUPER_ADMIN] }
        - { path: ^/articleType, roles: [ROLE_SUPER_ADMIN] }
        - { path: ^/users/roles, roles: [ROLE_SUPER_ADMIN] }
        
        - { path: ^/users/user/.*/changePass, roles: [ROLE_USER] }
        - { path: ^/users, roles: [ROLE_ADMIN] }
        
```

Setup database tables

```
bin/console doctrine:schema:update --force
```

then add roles

```
bin/console user:insert:role ROLE_SUPER_ADMIN "Super admin"
bin/console user:insert:role ROLE_ADMIN "Administrator"
bin/console user:insert:role ROLE_User "User"
```

insert your root user (username, password, email)

```
bin/console user:add panos panos panos@salix.gr
bin/console user:promote panos ROLE_USER_ADMIN
```

Now you can login to the application

