<?php

namespace Slx\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\UserBundle\Entity\User;
use Slx\UserBundle\Security\User\SlxUser;

class UserAddCommand extends ContainerAwareCommand {

    protected function configure() {
        $this
                ->setName('user:add')
                ->setDescription('Add a new user')
                ->addArgument('username', InputArgument::REQUIRED, 'Username')
                ->addArgument('email', InputArgument::REQUIRED, 'email')
                ->addArgument('password', InputArgument::REQUIRED, 'Password')
                ->addArgument('user_type', InputArgument::OPTIONAL, 'User type 0=user 1=admin 2=panos',0)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $username = $input->getArgument('username');
        $password = $input->getArgument('password');
        $email = $input->getArgument('email');
        $userType = $input->getArgument('user_type');

        $entity = new User();
        $entity->setUsername($username);
        $entity->setPassword($password);
        $entity->setEmail($email);
        $entity->setUserType($userType);
        $entity->setIsActive(true);
        
        $factory = $this->getContainer()->get('security.encoder_factory');
        $encoder = $factory->getEncoder(new SlxUser('', '', '', '', array()));
        $encodedPassword = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        $entity->setPassword($encodedPassword);

        $em = $this->getContainer()->get('doctrine')->getManager();
        $em->persist($entity);
        $em->flush();

        $output->writeln('Command result.');
    }

}
