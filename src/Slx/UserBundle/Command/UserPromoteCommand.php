<?php

namespace Slx\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UserPromoteCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:promote')
            ->setDescription('Adds a role to a user')
            ->addArgument('username', InputArgument::REQUIRED, 'Username')
            ->addArgument('role', InputArgument::REQUIRED, 'Role token')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $roleToken = $input->getArgument('role');
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        $user = $em->getRepository("SlxUserBundle:User")->findOneByUsername($username);
        if( $user ) {
            $role = $em->getRepository("SlxUserBundle:Role")->findOneByRole($roleToken);
            if( $role ) {
                // remove any existing roles
                foreach($user->getRoles() as $trole) {
                    $user->removeRole($trole);
                }
                // add new role
                $user->addRole($role);
                $em->persist($user);
                $em->flush();
                $output->writeln('Role added');
            }
            else {
                $output->writeln('Role not found');
            }
        }
        else {
            $output->writeln('User not found.');
        }
    }

}
