<?php

namespace Slx\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\UserBundle\Entity\Role;

class UserInsertRoleCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:insert:role')
            ->setDescription('Inserts role to roles table')
            ->addArgument('role', InputArgument::REQUIRED, 'Symfony security role token eg ROLE_ADMIN')
            ->addArgument('name', InputArgument::REQUIRED, 'Hunam readable name of the role eg Administrator')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $role = $input->getArgument('role');
        $name = $input->getArgument('name');

        $entity = new Role();
        $entity->setName($name);
        $entity->setRole($role);
        $em = $this->getContainer()->get('doctrine')->getManager();
        $em->persist($entity);
        $em->flush();

        $output->writeln('Command result.');
    }

}
