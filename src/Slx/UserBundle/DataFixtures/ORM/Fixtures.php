<?php
/**
 * @copyright 2017 Kartpay.com.
 * @author Panos <panos@kartpay.com>
 * Date: 15/10/2017
 * Time: 4:15 μμ
 */

namespace Slx\UserBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Slx\UserBundle\Entity\User;
use Slx\UserBundle\Entity\Role;
use Slx\UserBundle\Security\User\SlxUser;
use Slx\UserProfileBundle\Entity\UserProfile;

class Fixtures extends Fixture {
    public function load(ObjectManager $manager) {
        // add roles
        $rolesa = new Role();
        $rolesa->setName('Super Admin')->setRole('ROLE_SUPER_ADMIN');
        $manager->persist($rolesa);
        $role = new Role();
        $role->setName('Administrator')->setRole('ROLE_ADMIN');
        $manager->persist($role);
        $role = new Role();
        $role->setName('Χρήστης')->setRole('ROLE_USER');
        $manager->persist($role);
        $manager->flush();

        // add default user
        $user = new User();
        $user->setUsername('panos')
            ->setEmail('panos@salix.gr')
            ->setPassword('panos')
            ->setIsActive(true)
            ->setUserType(2)
            ;

        $factory = $this->container->get('security.encoder_factory');
        $encoder = $factory->getEncoder(new SlxUser('', '', '', '', array()));
        $encodedPassword = $encoder->encodePassword($user->getPassword(), $user->getSalt());
        printf("encoded=%s\n", $encodedPassword);
        $user->setPassword($encodedPassword);
        $user->addRole($rolesa);

        $profile = new UserProfile();
        $profile->setFirstname('Panos')->setLastname('panos')->setMobile('6973831076')->setPhone('6973831076')->setUser($user);
        $user->setProfile($profile);
        $manager->persist($profile);
        $manager->persist($user);

        $manager->flush();

    }
}