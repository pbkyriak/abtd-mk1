<?php

namespace Slx\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Slx\UserBundle\Entity\User;
use Slx\UserBundle\Form\UserType;
use Slx\UserBundle\Form\UserNewType;
use Slx\UserBundle\Security\User\SlxUser;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Slx\UserProfileBundle\Entity\UserProfile;

/**
 * User controller.
 *
 * @Route("/user")
 * @Breadcrumb("slx_metronic.general.home", route="slx_metronic_homepage")
 * @Breadcrumb("slx_metronic.menu.settings", route="user")
 * @Breadcrumb("users.user.users", route="user")
 * @CurrentMenuItem("user")
 */
class UserController extends Controller
{

    /**
     * Lists all User entities.
     *
     * @Route("/", name="user")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $form = $this->getFilterForm();
        $query = $this->makeListQuery($form,$request);
        if ($request->get('out') == 'csv') {
            $entities = $query->getResult();
            $response = $this->render('SlxUserBundle:User:index.csv.twig',
                array(
                'entities' => $entities
            ));
            $response->headers->set('Content-Type', 'text/csv');
            $date = new \DateTime();
            $response->headers->set('Content-Disposition',
                'attachment; filename="' . $date->format("U") . '-machines.csv"');
            return $response;
        }
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1) /* page number */,
            $request->query->get('limit', 15)
        );

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * Create list query
     */
    private function makeListQuery($form, $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM SlxUserBundle:User a";
        $form->handleRequest($request);
        $filterQryParams = array();
        $where = array();
        $request->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['title']) {
                $where[] = " a.title like :atitle ";
                $filterQryParams['atitle'] = str_replace('*', '%',
                    $filterData['title']);
            }
        }
        if ($where) {
            $dql .= ' where ' . implode(' and ', $where);
        }

        $query = $em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }

    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'title', \Symfony\Component\Form\Extension\Core\Type\TextType::class,
                array('required' => false, 'label' => 'slx_metronic.user.title')
            )
            ->getForm();
        return $form;
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/", name="user_create")
     * @Method("POST")
     * @Template("SlxUserBundle:User:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new User();
        $profile = new UserProfile();
        $entity->setProfile($profile);
        $form = $this->createForm(UserNewType::class, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder(new SlxUser('', '', '', '', array()));
            $encodedPassword = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
            $entity->setPassword($encodedPassword);

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'slx_metronic.general.record_saved');
            if ($form->get('saveAndAdd')->isClicked())
                return $this->redirect($this->generateUrl('user_new'));
            else
                return $this->redirect($this->generateUrl('user_show',
                            array('id' => $entity->getId())));
        }
        else {
            $this->get('session')->getFlashBag()->add('notice',
                'slx_metronic.general.record_errors');
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/new", name="user_new")
     * @Method("GET")
     * @Template()
     * @Breadcrumb("Προσθήκη")
     */
    public function newAction()
    {
        $entity = new User();
        $profile = new UserProfile();
        $entity->setProfile($profile);

        $form = $this->createForm(UserNewType::class, $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     * @Template()
     * @Breadcrumb("Προβολή")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        return array(
            'entity' => $entity,
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method("GET")
     * @Template()
     * @Breadcrumb("Μεταβολή")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createForm(UserType::class, $entity);

        return array(
            'entity' => $entity,
            'form' => $editForm->createView(),
        );
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}/update", name="user_update")
     * @Method("POST")
     * @Template("SlxUserBundle:User:edit.html.twig")
     * @Breadcrumb("Μεταβολή")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createForm(UserType::class, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'slx_metronic.general.record_saved');
            if ($editForm->get('saveAndAdd')->isClicked())
                return $this->redirect($this->generateUrl('user_new'));
            else
                return $this->redirect($this->generateUrl('user_show',
                            array('id' => $id)));
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'slx_metronic.general.record_errors');
        }


        return array(
            'entity' => $entity,
            'form' => $editForm->createView(),
        );
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}/delete", name="user_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SlxUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        if ($id == 1) {
            $this->get('session')->getFlashBag()->add('info',
                'users.user.cannot_delete_1');
            return $this->redirect($this->generateUrl('user_show',
                        array('id' => $id)));
        }
        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('user'));
    }

    /**
     * changes user password a User entity.
     *
     * @Route("/{id}/changePass", name="user_changePass")
     * @Method({"GET", "POST"})
     * @Template()
     * @Breadcrumb("Αλλαγή password")
     */
    public function changePassAction(Request $request, $id)
    {
        $returnToShow = false;
        if ($id) {
            $returnToShow = true;
        } else {
            $id = $this->getUser()->getId();
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SlxUserBundle:User')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('users.user.not_found');
        }

        $form = $this->createFormBuilder()
            ->add(
                    'password', RepeatedType::class, 
                    array(
                        'type' => TextType::class,
                        'invalid_message' => 'The password fields must match.',
                        'options' => array('attr' => array('class' => 'password-field')),
                        'required' => true,
                        'first_options'  => array('label' => 'users.user.password'),
                        'second_options' => array('label' => 'users.user.repeat_password'),
                    )
            )
            ->add('save', SubmitType::class,
                array('label' => 'slx_metronic.general.save', 'attr' => array('class' => 'btn blue')))
            ->getForm();

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder(new SlxUser('', '', '', '', array()));
                $encodedPassword = $encoder->encodePassword($data['password'], $entity->getSalt());
                $entity->setPassword($encodedPassword);
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('info', 'users.user.password_changed');
                if ($returnToShow) {
                    return $this->redirect($this->generateUrl('user_show', array('id' => $id)));
                } else {
                    return $this->redirect($this->generateUrl('slx_metronic_homepage'));
                }
            }
        }

        return array(
            'form' => $form->createView(),
            'uid' => $returnToShow ? $id : 0,
        );
    }

}
