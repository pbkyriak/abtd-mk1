<?php

namespace Slx\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Slx\UserBundle\Entity\Role;
use Slx\UserBundle\Form\RoleType;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

/**
 * Role controller.
 *
 * @Route("/roles")
 * @Breadcrumb("Home", route="slx_metronic_homepage")
 * @Breadcrumb("Χρήστες", route="user")
 * @Breadcrumb("Roles")
 * @CurrentMenuItem("roles")
 */
class RoleController extends Controller
{

    /**
     * Lists all Role entities.
     *
     * @Route("/", name="roles")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $form = $this->getFilterForm();
        $query = $this->makeListQuery($form,$request);
        if ($request->get('out') == 'csv') {
            $entities = $query->getResult();
            $response = $this->render('SlxUserBundle:Role:index.csv.twig',
                array(
                'entities' => $entities
            ));
            $response->headers->set('Content-Type', 'text/csv');
            $date = new \DateTime();
            $response->headers->set('Content-Disposition',
                'attachment; filename="' . $date->format("U") . '-machines.csv"');
            return $response;
        }
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1) /* page number */,
            $request->query->get('limit', 15)
        );

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * Create list query
     */
    private function makeListQuery($form,$request)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM SlxUserBundle:Role a";
        $form->handleRequest($request);
        $filterQryParams = array();
        $where = array();
        $request->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['title']) {
                $where[] = " a.title like :atitle ";
                $filterQryParams['atitle'] = str_replace('*', '%',
                    $filterData['title']);
            }
        }
        if ($where) {
            $dql .= ' where ' . implode(' and ', $where);
        }

        $query = $em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }

    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'title', TextType::class,
                array('required' => false, 'label' => 'slx_metronic.role.title')
            )
            ->getForm();
        return $form;
    }

    /**
     * Creates a new Role entity.
     *
     * @Route("/", name="roles_create")
     * @Method("POST")
     * @Template("SlxUserBundle:Role:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Role();
        $form = $this->createForm(RoleType::class, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'slx_metronic.general.record_saved');
            if ($form->get('saveAndAdd')->isClicked())
                return $this->redirect($this->generateUrl('roles_new'));
            else
                return $this->redirect($this->generateUrl('roles_show',
                            array('id' => $entity->getId())));
        }
        else {
            $this->get('session')->getFlashBag()->add('notice',
                'slx_metronic.general.record_errors');
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Role entity.
     *
     * @Route("/new", name="roles_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Role();
        $form = $this->createForm(RoleType::class, $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Role entity.
     *
     * @Route("/{id}", name="roles_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxUserBundle:Role')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Role entity.
     *
     * @Route("/{id}/edit", name="roles_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxUserBundle:Role')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $editForm = $this->createForm(RoleType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Role entity.
     *
     * @Route("/{id}/update", name="roles_update")
     * @Method({"PUT", "POST"})
     * @Template("SlxUserBundle:Role:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxUserBundle:Role')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(RoleType::class, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'slx_metronic.general.record_saved');
            if ($editForm->get('saveAndAdd')->isClicked())
                return $this->redirect($this->generateUrl('roles_new'));
            else
                return $this->redirect($this->generateUrl('roles_show',
                            array('id' => $id)));
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'slx_metronic.general.record_errors');
        }


        return array(
            'entity' => $entity,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Role entity.
     *
     * @Route("/{id}", name="roles_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SlxUserBundle:Role')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Role entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('roles'));
    }

    /**
     * Creates a form to delete a Role entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                ->add('id', HiddenType::class)
                ->getForm()
        ;
    }

}
