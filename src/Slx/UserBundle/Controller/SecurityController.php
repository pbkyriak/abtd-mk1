<?php

namespace Slx\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityController extends Controller
{

    public function loginAction(Request $request)
    {
        /*
        echo 'user1';
        $factory = $this->get('security.encoder_factory');
        $user = new \Slx\UserBundle\Entity\User();
        $user->setUsername('panos');
        $user->setEmail('panos@salix.gr');
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword('panos', $user->getSalt());
        $user->setPassword($password);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        */
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        
        return $this->render(
            'SlxUserBundle:Security:login.html.twig',
            array(
                'last_username' => $lastUsername,
                'error' => $error,
            )
        );
    }

    public function securityCheckAction()
    {
        // The security layer will intercept this request
        die('Wrong security configuration!');
    }

    public function logoutAction()
    {
        // The security layer will intercept this request
    }
}